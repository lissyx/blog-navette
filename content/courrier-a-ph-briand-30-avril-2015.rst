courrier a ph BRIAND 30 avril 2015
##################################
:date: 2015-08-10 06:49
:category: Courriers Institutions


:slug: courrier-a-ph-briand-30-avril-2015
:status: published

Union Départementale CFDT d’Indre et Loire « La Camusière » 18 ruede l’oiselet 37550 Saint Avertin

Tél : 02 47 36 58 58 Fax : 02 47 36 58 51 Email :indre-loire@centre.cfdt .fr

Tours le 30 avril 2015

Objet : Desserte des gares de Tours – St-Pierre des Corps Par navette –Projet CFDT

Tours Plus 37000 - Tours M Philippe BRIAND

Monsieur le Président,

Une nouvelle fois nous revenons vers vous dans ce dossier afin d’obtenirsoit un rendez-vous pour vous présenter notre projet, soit pour une réponse ànotre courrier transmis en juillet 2014.

Depuis des années, les usagers de la SNCF rencontrent des difficultés pourrejoindre les gares de Tours ou St-Pierre des Corps en raison de la suppressiondes navettes. Aujourd’hui, il faut parfois compter un temps d’attente et deparcours entre ces 2 gares proche des 15 à 20 minutes qui viennent doncs’ajouter au temps de trajet.

Même si à ce jour, l’enquête publique de la révision du PDU pour les 10prochaines années est considérée comme bouclée, pour autant l’UnionDépartementale CFDT a souhaiter porter au débat le sujet crucial de la desserteentre les gares de Tours et St-Pierre des Corps et cela depuis décembre 2013,dans le cadre d’un projet de desserte sur voie dédiée économe en termes detemps, d’espace, un projet de circulation économe en énergie. Cettecontribution pourrait faire partie des ajustements du projet de PDU.

La proposition de la CFDT permettrait à la fois de faciliter lesacheminements entre les 2 gares pour les usagers de la SNCF, mais également dedésenclaver la Gare de St-Pierre des Corps en offrant des relations rapides àla population du quartier de la gare de St-Pierre des Corps, pour se rendre àTours.

Proposition CFDT : afin de relier les 2 gares par un système léger etfonctionnel sur voie dédiée, il serait possible ;

1) D’utiliser une voie n°8 (repères B et C) de la SNCF qui ne sert plusactuellement, située le long des résidences hôtelières de la rue Edouardvaillant. La plateforme de cette voie débute au niveau de la gare de Tours prèsdu passage d’accès de la rue Edouard Vaillant (accès minute) (photos 4 24) etse termine sur un « buttoir » situé lui à quelques dizaines de mètresdu pont de la rue Edouard vaillant (au niveau de l’arrêt Fil-Bleu Jolivet -photo 24). La présence de cette voie et donc de la plateforme existe, elle estdisponible au moins jusqu’au pont de la Place Jolivet.

2) Pour permettre des croisements entre 2 rames, il serait possibled’aménager une portion de 2 éme voie dans la pointe des installations duCE-SNCF (les Peupliers) dont la SNCF/RFF disposent d’un terrain en fricheactuellement.

3) Il conviendrait aussi d’élargir le pont de la rue Edouard Vaillant pourpermettre l’aménagement d’une nouvelle voie. (Photos 18 à 23)

4) D’utiliser les emprises disponibles de la SNCF ou de RFF le long de larue Georges Collon (dont les maisons vont être détruites), il n’y aurait doncpas d’expropriation. La SNCF/RFF dispose d’une piste le long de cette artère etl’élargissement ne poserait pas de problèmes particuliers pour agrandir laplateforme (repères D – Photos 30 à 33 et 41 à 43)

5) Le passage sous le pont de l’autoroute ne doit pas poser de problème nonplus du fait des espaces disponibles d’autant que la piste SNCF/RFF y estprésente. Il serait donc possible de prolonger cette voie en utilisant unepartie des emprises située sous le pont de l’autoroute et de la prolongerau-dessus du (tube/tunnel) (Photos 46 à 49 repères D et E).

6) Afin de prolonger la voie il serait possible d’utiliser les emprises dela SNCF/RFF au niveau du parking du technicentre de St-Pierre des Corps puis auNord de ce parking l’espace qui est toujours disponible (là où la SNCF détenaitpar le passé un foyer d’accueil « dit du canal ») (Repère G et H – Photos38 à 40), terrain qui n’est pas utilisé. Cela amènerait à 200/300 mètres de lagare de St-Pierre des Corps, au niveau du transformateur situé au niveau desvoies d’accès du technicentre (repère T).

Reste à résoudre au niveau des emprises les quelques 200 à 300 mètres quipermettraient d’accéder directement à la gare de St-Pierre, pour lesquels ilfaudra rechercher des solutions techniques (repère E et F).

Sur l’ensemble de ce tracés, au niveau des emprises, dès lors que RFFs’accorderait soit à réaliser les travaux, soit à concéder cette partie del’infrastructure, les seules véritable difficultés résident sur l’élargissementdu pont de la rue Edouard Vaillant (au niveau de la place Jolivet), le passagesous l’autoroute et sur les quelques derniers mètres d’accès à la gare deSt-Pierre des Corps.

Selon la CFDT, il serait donc possible de réaliser à un coût raisonnable uneligne pour l’aménagement d’un système de transport sur voie ferrée dédiée poury faire circuler un type de rames banlieue rénovées (aujourd’hui placées survoies de garages, donc inutilisées) ou avec du matériel plus léger qui reste àdéfinir (tram, tram-train, métro) entre les deux gares, ce point resterait dela responsabilité du gestionnaire de l’infrastructure (RFF) et de l’exploitantqui sera retenu dans le cadre de ce projet.

Pour ce faire, il conviendra aux différents acteurs qui auraient en chargece projet, dans un premier temps de lancer une étude de faisabilité (pour sescompétences, l’Atelier d’Urbanisme de l’agglo pourrait se saisir de ce dossier)et par la suite, de réaliser un tour de table financier afin de réunir lesfonds nécessaire aux investissements. A cette fin, l’agglo pourrait recourir àmettre en place une « surtaxe locale temporaire », voir le document jointen annexe.

En espérant que cette contribution trouvera écho auprès des élus, de la SNCFet de RFF, nous sommes prêts à vous présenter notre projet dont la presse s’estfait l’écho à plusieurs reprises et qui a reçu l’aval des associations du TGVet à débattre avec vous et vos collaborateurs de ce sujet en partenariat avecla SNCF et/ou RFF.

Il va de soi que pour la CFDT ce système reliant les 2 gares n’a pasvocation ;

a) à s’opposer ou à retarder la réalisation d’une 2 éme ligne de tramwaypour desservir St-Pierre des Corps et la Ville aux Dames à la Riche.

b) à remettre en cause les projets de réhabilitation de l’étoileferroviaire.

Dans l’attente de vous lire ou de vous rencontrer, recevez Madame Monsieur,l’expression de notre haute considération.

Copie transmise à : M Jean Yves COUTEAU Président du ConseilDépartemental d’Indre et Loire M Philippe Brian Président de Tour(s) Plus

Pièces jointes : Différentes photos correspondant au tracé - CD Schémades infrastructures - CD Surtaxe locale temporaire

Pour l’UD – CFDT – 37 Le gestionnaire du Projet

Daniel BERNARD également membre du CESER Centre Val de Loire

.. raw:: html

   </p>
