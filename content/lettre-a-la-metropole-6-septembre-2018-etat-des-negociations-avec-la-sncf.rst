Lettre à la métropole, 6 septembre 2018 : État des négociations avec la SNCF
############################################################################
:date: 2018-09-10 09:33
:category: Courriers CFDT


:slug: lettre-a-la-metropole-6-septembre-2018-etat-des-negociations-avec-la-sncf
:status: published

Cet été devait se tenir, le 13 juillet, une réunion pour faire le point.Celle-ci a été reportée à la demande du représentant de la métropole qui étaiten attente d'une réunion avec SNCF Réseau. Il semble que cette réunion ait eulieu, et il est donc temps de faire le point sur l'état des discussion.

`Lettre à la Métropole, 6septembre 2018 <{static}/docs/lettre_metrop_6_sept_2018.pdf>`__

.. raw:: html

   </p>
