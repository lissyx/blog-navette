article NR navettes - tramway, le CRTE réagit
#############################################
:date: 2016-01-21 16:25
:category: Presse et médias


:slug: article-nr-navettes-tramway-le-crte-reagit
:status: published

Le CRTE\* - CFDT se positionne

Le CRTE-CFDT (comité Régional des Transports et de l’Environnement) – CFDT aréagi aux articles parus ces derniers jours autour de la deuxième ligne detramway :

Nous sommes bien entendu favorable à l’extension du réseau de tram afinqu’il fasse système avec le pôle de mobilité situé à la gare de Tours (train,tram, bus, autocars, etc..). il ne faut pas perdre trop de temps dans laréflexion, car il est nécessaire de réserver du foncier et de concevoir lesréseaux (eau, gaz, électricité, câble, routes et voiries, etc..) poursimplifier les procédures et les travaux à mettre en œuvre au niveau del’infrastructure et des obligations administratives et ainsi réduire les délaiset les coûts.

Dans ce débat, nous sommes toutefois surpris de ne pas voir apparaitre dansles différentes expressions, la complémentarité que pourrait apporter le projetde navettes entre les gares de Tours et de St-Pierre des Corps, projet portépar la CFDT.

l’ADTT entend entre autre desservir la gare TGV par un tramway. De plus lesconstructions réalisées aux abords directs de la gare (hôtels, bureaux) neseraient pas sans poser de problèmes pour faire du quai à quai comme en gare deTours. Ce quartier connait d’ailleurs bien depuis des années des difficultésd’accès à la gare et aux riverains.

Pour la CFDT, si ce projet parait intéressant pour desservir à termecertaines populations de St-Pierre des corps (voir de la Ville aux dames), ilne répond pas à l’attente des usagers du TGV et à une desserte rapide entre lesdeux gares en site propre en 5 minutes maximum.

-  CRTE-CFDT (comité Régional des Transports et de l’Environnement) CFDT

Ce Comité regroupe l’ensemble des syndicats des transports et del’Environnement (routiers, cheminots, urbains, autocars, aérien, autoroutes,etc…).

Plus d’infos : Blog.navette-tours-saintpierre.fr Ou sur notre pageFacebook : Cfdt-Crte navettes Tours st Pierre

.. raw:: html

   </p>
