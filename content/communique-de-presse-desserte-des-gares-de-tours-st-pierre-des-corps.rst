Communiqué de presse : Desserte des gares de Tours – St-Pierre des Corps
########################################################################
:date: 2015-08-09 14:10
:category: Presse et médias


:slug: communique-de-presse-desserte-des-gares-de-tours-st-pierre-des-corps
:status: published

Union Départementale CFDT d’Indre et Loire « La Camusière » 18 ruede l’oiselet 37550 Saint Avertin

Tél : 02 47 36 58 58 Fax : 02 47 36 58 51 Email :indre-loire@centre.cfdt .fr

Tours le 17 juin 2015

Objet : Desserte des gares de Tours – St-Pierre des Corps

::

                                    Communiqué de presse de l’UD – CFDT

Monsieur,

A la CFDT, nous sommes quelques surpris, pour ne pas dire outrager par laposition du sieur François Bataille dans la nouvelle république d’Indre etLoire du 17 juin 2015 (page 7), dont on ne sait qui il représente et à queltitre il s’exprime !

En effet, notre projet est construit et nous ne parlons dans le coût que dumontant des infrastructures, pas du matériel, du ressort de l’exploitant.

D’autre part, il semble que ce Monsieur, ne connait pas vraiment le contenude notre projet, en effet ;

1) Il est hors de question d’utiliser une des voies de lavages construitepour la compensation des travaux du tramway, mais d’utiliser une voieactuellement inutilisée. 2) Nous n’avons jamais caché qu’il fallait élargir letablier du pont de la place Jolivet pour y déposer une voie supplémentaire etcela ne devrait pas avoir (au regard du site des répercussions sur lescirculations ferroviaires et routières. 3) Quant au passage sous l’A10, nousavons dans notre projet proposé qu’une voie soit aménagée au Nord des piles desponts actuels, le fret passant lui au sud. 4) Quant à l’arrivée à St-Pierrenous proposons non pas d’arriver voie 4 avec un éventuel cisaillement de lavoie 6, mais d’aménager une voie le long du parcotrain avec un quai en accèsdirect avec le hall de la gare.

Si nous comprenons bien, il ne faudrait rien faire et laisser la situationen l’état alors que notre projet, permettrait à la fois les relations entre les2 terminaux SNCF de St-Pierre des Corps ou d’être relier par une voie rapidedédiée et dans le même temps faciliter l’accessibilité des habitants deST-Pierre pour Tours, ou l’inverse.

Pour notre part nous sommes prêts à échanger sur ce sujet, dès lors qu’enface de nous nous aurions des interlocuteurs intéressés ; comme ce fut lecas avec la Mairie de Tours et avec le Député J.P Gille.

Pour l’UD – CFDT

Daniel BERNARD

.. raw:: html

   </p>
