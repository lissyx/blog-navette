Navette Tours-Saint Pierre-des-Corps : la Métropole lance deux études !
#######################################################################
:date: 2019-01-10 16:38
:category: Presse et médias


:slug: navette-tours-saint-pierre-des-corps-la-metropole-lance-deux-etudes
:status: published

Comme demandé par la CFDT et récemment confirmé par le Vice-Président encharge des mobilités au sein de la Métropole, Frédéric Augis, deux études ontété lancées, l'une par la SNCF pour la faisabilité des navettes, et l'autre surun lien mixte ferroviaire / urbain.

La CFDT reste disponible et prudente.

`communique-presse-18-decembre-2018Communiquéde presse du 18 décembre 2018 <{static}/docs/com_presse_du_18_dec_2018.pdf>`__

.. raw:: html

   </p>
