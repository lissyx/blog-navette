Lettre à l'agglomération, 30 mai 2016
#####################################
:date: 2016-05-31 12:43
:category: Courriers Institutions


:slug: lettre-a-lagglomeration-30-mai-2016
:status: published

Lettre envoyée aux représentants de l'agglomération Tour(s)Plus le 30 mai2016, à propos des derniers développements autour de la liaison entre Tours etSaint-Pierre-des-Corps et réaffirmer l'option ferroviaire : `lettre-agglo-30-mai-2016 <{static}/docs/lettre_a_l_agglo_du__30_mai_2016-1.pdf>`__

.. raw:: html

   </p>
