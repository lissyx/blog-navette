tram et ADTT
############
:date: 2016-01-23 10:49
:category: Presse et médias


:slug: tram-et-adtt
:status: published

NR du 23/01/2016

Deuxième ligne de tram : il faut faire vite

Environ 80 personnes ont assisté à la réunion de l'ADTT. - Environ 80personnes ont assisté à la réunion de l'ADTT.

Sur le tramway, apprendre des autres. Tel était le thème de la réunionpublique organisée jeudi soir, aux halles, par l'Association pour ledéveloppement du transport collectif en Touraine (ADTT). Devant quelque 80personnes, le président de l'ADTT, Jean-François Troin, a rappelé la nécessitéde ne pas traîner pour le lancement d'une deuxième ligne, car « entre laprise de décision et la mise en service, cinq à six années sont nécessaires ».« Nous sommes dans une période de non-anticipation ou de longueshésitations », a-t-il toutefois reconnu.

Pour alimenter le débat, l'association avait invité des intervenantsd'Angers, du Mans et d'Orléans, trois agglomérations de taille comparable àcelle de Tours où la deuxième ligne est en projet (Angers), ou opérationnelle(Le Mans depuis avril 2014, Orléans depuis juin 2012). « Sur les 25tramways en France, seule la Ville d'Aubagne a une seule ligne ; tous lesautres ont au moins un embranchement », a rappelé Jean-François Troin. Lesparticipants ont ensuite découvert les différents tracés que proposel'association : l'embranchement en Y partant du carrefour de Verdun pourrejoindre Trousseau, « la plus simple » ; la ligne est-ouest, entre lagare TGV de Saint-Pierre-des-Corps et Bretonneau, « un choix qui créeraitun véritable réseau avec cinq terminus et desservirait la Rabâterie, seulquartier prioritaire de l'agglo non traversé par le tram à ce jour. » Dansl'assistance, Emmanuel Denis (conseiller municipal écologiste à Tours) a estiméque « l'idée d'un périphérique est encore défendue par certains, il fautrester vigilant » ; « On parle de 3, 4 voire 5 lignes à cinquante ans maisen attendant on n'avance pas », a ajouté le député Jean-Patrick Gille. Adjointau maire de la Riche en charge de l'urbanisme, Daniel Langé a mis en avant lanécessité de penser à long terme : « Le tram a deux objectifs : relier lespoints existants et porter un élan sur les futures zones à desservir. »« Que 2016 soit l'année du lancement de la deuxième ligne », a concluJean-François Troin. N. R.

.. raw:: html

   </p>
