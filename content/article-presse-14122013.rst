article presse 14/12/2013
#########################
:date: 2015-08-10 06:45
:category: Presse et médias


:slug: article-presse-14122013
:status: published

La CFDT veut remettre les navettes sur les rails - 14/12/2013 05:38

Les correspondances entre les gares de Tours et Saint-Pierre-de-Corpspourraient facilement être rétablies selon le syndicat. Éléments dudossier.

Les syndicalistes CFDT proposent une solution simple pour rétablir lesnavettes entre les gares de Tours et Saint-Pierre-des-Corps.

Depuis décembre 2011, les navettes systématiques entre les gares de Tours etSaint-Pierre-des-Corps ont disparu, pour ne pas couper la voie aux trainsgrandes lignes. Les usagers s'en plaignent mais aucune solution n'a étéapportée. L'union départementale CFDT, en lien avec son comité régional destransports et de l'environnement, a planché sur le dossier et envisagé unprojet qu'elle estime « réaliste », sans toutefois apporter de chiffrage.(Voir la carte interactive)

« Dire que Tours est à une heure de Paris et mettre plus de vingtminutes à rallier Saint-Pierre, c'est un comble, souligne Daniel Bernard,retraité cheminot. Notre projet s'appuie sur des voies existantes et desemprises déjà sur le domaine ferroviaire. » Il s'agit donc, pour lesyndicat, de passer à gauche de l'actuelle voie A1 à Tours, puis de filer versune voie existante mais inutilisée. « Il faudrait élargir la plateformepour passer le pont de la rue Edouard-Vaillant à Tours et passer au nord despiles du pont de l'A 10 vers le technicentre de saint-Pierre-des-Corps, ilfaudrait déplacer des transformateurs électriques, mais aucun réseau d'eau oude gaz n'est à bouger. » Un kilomètre de voie coûte un million d'euros :il faudrait donc compter 2,7 M€ pour les voies, plus les budgets pour lespassages d'ouvrages d'art. « Nous ne nous engageons pas sur l'opérateur nile mode de transport, souligne Claude Garou, secrétaire national de la branchetransport. Nous envoyons notre projet aux responsables politiques, à la Région,au Département, à Tour(s) plus comme aux associations d'usagers et à la SNCF etRFF. » La récente publication du plan de déplacements urbains et lesélections municipales qui se profilent devraient permettre d'ouvrir le débat.C'est le souhait des responsables de la CFDT. « Notre solution est moinschère que toutes les autres, souligne Daniel Bernard. Elle ne nécessite niexpropriation, ni grosse infrastructure. C'est la solution idéale ! »

Le parcours qu'emprunteraient ces navettes :

::

      En rose, les espaces existants   En vert les espaces à aménager ou élargir

.. raw:: html

   </p>
