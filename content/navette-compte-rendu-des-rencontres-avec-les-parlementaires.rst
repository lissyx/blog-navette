Navette : compte-rendu des rencontres avec les parlementaires
#############################################################
:date: 2018-04-26 18:13
:category: Presse et médias


:slug: navette-compte-rendu-des-rencontres-avec-les-parlementaires
:status: published

Différents parlementaires ont été rencontrés récemment au sujet du projet denavette (Philippe CHALUMEAU le 2 mars ; Serge BABARY le 13 avril, SophieAUCONIE le 16 avril). Tous partagent l'idée de l'efficacité du projet proposé,et confirment qu'il ne remet pas en cause la dessert de Saint-Pierre-des-Corpspar le tram : celle-ci devra se faire, mais l'échéance et les contraintesde la navette ne sont pas compatibles, et il convient de dissocier ces deuxprojets. `Communiqué de Pressedu 26 avril 2018 <{static}/docs/com_presse_du_26_avril_2018.pdf>`__

.. raw:: html

   </p>
