communiqué de presse 12 sept 2017
#################################
:date: 2017-09-18 11:45
:category: Courriers CFDT


:slug: communique-de-presse-12-sept-2017
:status: published

Tours le 12 septembre 2017

Communiqué de Presse

Objet : Navettes Tours – St-Pierre des Corps

Monsieur le Rédacteur en Chef,

Suite à différents articles dans plusieurs organes de presse du départementconcernant les projets d’extension du Tram et notamment ceux concernant ladesserte entre les 2 gares de Tours et de St-Pierre, l’UD CFDT a sollicité enJuin la Métropole afin de faire un point sur l’avancement de ce dossier.

C’est dans ces conditions que l’UD a été reçue le 12 septembre 2017, parFrédéric AUGIS, vice-Président de la Métropole en charge des transports et dela Responsable du service.

En l’absence de Guy SIONNEAU, la Délégation CFDT était composée de DanielBERNARD (pilote du projet de navettes) et de Jean-Paul CARLAT (cheminotretraité).

Après avoir rappelé l’importance d’une desserte rapide, fiable, lisible etfonctionnelle entre les 2 gares s’inscrivant dans le développement desmobilités, l’essor économique, touristique, nous avons regretté le retard prisdans ce domaine.

Le premier tour de table a été consacré aux infos qui circulent (souvent endehors de la métropole), F AUGIS, a confirmé sa volonté de poursuivre avec laCFDT dans la transparence la réflexion engagée, sur la base de notreprojet.

Toutefois, notre projet ne pourrait aboutir qu’avec la volonté de la SNCF deconcéder une partie de ses emprises ferroviaires inutilisées depuis plus de 20ans.

La Métropole doit de nouveau rencontrer d’ici à fin septembre les nouveauxreprésentants de la SNCF (SNCF Réseau et le Directeur Régional de la SNCF MCOURSIER) afin d’aborder ce sujet. Un premier comité technique se tiendra d’icifin septembre, puis un comité de pilotage. Une nouvelle rencontre seraprogrammée avant la fin de l’année entre la Métropole et la CFDT et peut-êtreSNCF Réseau pour clarifier les différentes hypothèses.

La métropole nous a indiqué vouloir prendre une décision rapide sur lesujet, avec 2 hypothèses ;

- Soit la SNCF s’engage aux côtés de la Métropole pour un projet commun, -Soit la métropole en tirera les conclusions et proposera une autre solutionmoins rapide, plus couteuse certes, mais en dehors des emprisesferroviaires.

A la CFDT, nous restons sceptiques sur la volonté de la SNCF d’aboutir à unprojet ferroviaire, comme nous l’avons déjà indiqué, « tout ce qui nepasse pas par le fer a l’agrément de la SNCF » c’était le credo del’ancien directeur M BORRI. Les mentalités ont-elles changées ?

En tout état de cause, la Métropole à un sérieux atout en main, avec lesnouvelles compétences transports transférer aux métropoles avec notamment lagestion des gares.

Pour conclure,

"L’UD – CFDT est soucieuse d’améliorer le service rendu aux utilisateurs enpréservant la dépense publique par l’utilisation des potentiels existants.

L’UD CFDT se félicite des relations d’écoute et de travail entre laMétropole et notre OS dans le cadre de ce projet, pris très au sérieux par lesélus

Afin de faire connaitre notre projet et de recueillir les avis, la CFDT acréé ; - Une page Facebook à l’adresse suivante : CFDT-CRTE navettestours St pierre - avec un lien vers le blog

- Où à partir de Google ou de Yahoo ; Un blog à l’adressesuivante : - blog.navette-tours-saintpierre.fr - Ou :http://navette-tours-saintpierre.fr/

Dans l’attente de vous lire ou de vous rencontrer, recevez Madame Monsieur,l’expression de notre haute considération.

Pour l’UD – CFDT – 37 Le gestionnaire de ces Projets

Daniel BERNARD

.. raw:: html

   </p>
