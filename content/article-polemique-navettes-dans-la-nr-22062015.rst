article polémique navettes dans la NR 22/06/2015
################################################
:date: 2015-08-10 06:10
:category: Presse et médias


:slug: article-polemique-navettes-dans-la-nr-22062015
:status: published

Navette SNCF : le débat est relancé - 22/06/2015 05:26

Il fallait s'y attendre, le débat lancé dans nos colonnes, lundi dernier,autour de la proposition de la CFDT d'installer une navette SNCF entre Tours etSaint-Pierre-des-Corps a suscité une première réaction négative d'un lecteur.Lequel lecteur a surtout mis en cause le coût estimatif avancé par la CFDT,entre 10 et 15 M€, quand lui parlait d'un chiffre quatre à cinq fois supérieurs(lire NR du 17 juin).

« A la CFDT, nous sommes quelque peu surpris, pour ne pas dire outragéspar la position de François Bataille », réplique Daniel Bernard. « Notreprojet est construit et nous ne parlons dans le coût que du montant desinfrastructures, pas du matériel, du ressort de l'exploitant. D'autre part, ilsemble que ce monsieur ne connaisse pas vraiment le contenu de notre projet.Car il est hors de question d'utiliser une des voies de lavage construite pourla compensation des travaux du tramway, mais d'utiliser une voie actuellementinutilisée. Par ailleurs, nous n'avons jamais caché qu'il fallait élargir letablier du pont de la place Jolivet pour y déposer une voie supplémentaire etcela ne devrait pas avoir (au regard du site) de répercussions sur lescirculations ferroviaires et routières. Quant au passage sous l'A 10, nousavons proposé qu'une voie soit aménagée au nord des piles des ponts actuels, lefret passant lui au sud. Pour l'arrivée à Saint-Pierre, nous proposons nond'arriver voie 4 avec un éventuel cisaillement de la voie 6, mais d'aménagerune voie le long du parcotrain avec un quai (directement en accès direct avecle hall de la gare. » Et la CFDT de se demander s'il faut rester les brascroisés. « Notre projet permettrait d'établir des relations entre les deuxterminaux SNCF de Saint-Pierre des Corps et de fluidifier la circulation despopulations entre Tours et Saint-Pierre. Nous sommes prêts à échanger sur cesujet, dès lors qu'en face de nous, nous aurions des interlocuteursconstructifs. »

Suivez-nous sur Facebook

.. raw:: html

   </p>
