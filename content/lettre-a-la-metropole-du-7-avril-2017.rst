lettre à la metropole du 7 avril 2017
#####################################
:date: 2017-04-08 16:02
:category: Courriers Institutions


:slug: lettre-a-la-metropole-du-7-avril-2017
:status: published

Union Départementale CFDT d’Indre et Loire « La Camusière » 18 ruede l’oiselet 37550 Saint Avertin

Tél : 02 47 36 58 58 Fax : 02 47 36 58 51 Email :indre-loire@centre.cfdt .fr

Tours le 7 avril 2017

M Philippe BRIAND Président de Tours Métropole Val de Loire

M Frédéric AUGIS Vice-Président transports 60, Avenue Marcel DASSAULT 37206- TOURS – Cedex 3

M Serge BABARY Maire de TOURS Conseiller communautaire

Monsieur,

Depuis plusieurs semaines, la presse relate les travaux de l’agglo,aujourd’hui de la Métropôle concernant le développement des transports urbainset les projets à l’étude avec dans un premier temps, la création d’une nouvelleligne de Tram qui raccorderait les deux pôles du CHRU.

Si pour nous ce premier projet et essentiel et indispensable, comme tenu desflux entre ces deux pôles, pour autant, l’amélioration de la desserte entre les2 gares de Tours et de St-Pierre des Corps l’est également.

Nous avons eu l’occasion de présenter à plusieurs reprises le projetenvisagé par l’Union Départementale CFDT qui semblait retenir votre attentionet pour lequel vous souhaitiez une étude indépendante.

A ce jour alors que des annonces doivent être divulguées d’ici quelquessemaines concernant l’évolution des lignes de Tram, nous n’avons aucun échoconcernant la desserte des deux gares et des populations entre de St-Pierre etde Tours.

Or, dans quelques mois, le TGV desservira depuis S-Pierre des CorpsBordeaux, mais également Poitiers, la Rochelle et Angoulême avec des temps deparcours raccourcis.

A cet effet une liaison rapide entre les 2 gares est plus que jamaisindispensable, surtout lorsque l’on veut assurer son rôle de Métropole.

Aussi, nous aimerions savoir comment évolue le traitement du dossier quenous avons déposé.

De plus, le changement de Direction à la tête de la SNCF en région Centreavec le départ de M BORRI et son remplacement par Stéphane Coursier devraitpermettre de reprendre un dialogue constructif sur ce sujet sur ce sujet.

Nous restons à l’entière disposition de l’agglo pour toutes rencontres et/ousuites utiles.

Monsieur le Président, Messieurs les Conseillers, au nom de l’UD – CFDT 37,nous vous présentons notre haute considération.

Notre projet en ligne sur notre blog

Blog.navette-tours-saintpierre.fr Ou sur notre page Facebook :Cfdt-Crte navettes Tours st Pierre

Pour l’UD – CFDT et son CRTE –CFDT Guy SIONNEAU - Claude GAROU

Pour le Pilotage du Projet Daniel BERNARD

.. raw:: html

   </p>
