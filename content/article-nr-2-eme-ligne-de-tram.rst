article NR 2 eme ligne de Tram
##############################
:date: 2017-07-08 06:54
:category: Presse et médias


:slug: article-nr-2-eme-ligne-de-tram
:status: published

Deuxième ligne de tram : comment relier les deux hôpitauxtourangeaux ? 07/07/2017 05:41 Nombre de commentaires réagir(1) Envoyerpar mail Imprimer Attendue pour 2025, la seconde ligne de tram ne coûtera pasmoins de 300 M&#8364;. - Attendue pour 2025, la seconde ligne de tram necoûtera pas moins de 300 M&amp;#8364;. Attendue pour 2025, la seconde lignede tram ne coûtera pas moins de 300 M&#8364;. Attendue pour 2025, laseconde ligne de tram ne coûtera pas moins de 300 M€.

Le Conseil de développement a eu droit hier à une analyse de la métropolesur le tram. Sur les vingt-deux tracés qui ont été présentés, un sembleprivilégié.

::

   Si chacun salue la beauté du tram et son succès, personne n'oublie qu'à Tours, il avait un train de retard. Et que les études sur la deuxième ligne auraient dû se faire dans la foulée de l'inauguration, en 2013. Le propos a été redit hier, dans les locaux de la métropole, devant le Conseil de développement de Tours Métropole pour la présentation des réflexions. Elles en sont au « stade 2 » (sur 3), c'est-à-dire au niveau des souhaits (contradictoires) des élus, du CHU, de l'université, des associations, avec, en plus, des indicateurs tels que les prévisions du nombre de voyageurs, le coût.

La phase 3 permettra d'être encore plus concret, voir de près lesexpropriations. L'analyse présentée par Laurence Marin, directrice destransports à la métropole, comprend beaucoup de prospective, à moyen et longterme (2040). Mais quoi qu'il en soit, Frédéric Augis, vice-président auxtransports, estime cette deuxième ligne opérationnelle en 2025.

Le boulevard Béranger séduisant

A ce stade, un élément ressort : la volonté de « desservir les hôpitaux». L'étude met en avant d'autres souhaits, fait ressortir les halles comme lieuemblématique, passer par les Fontaines et relier la gare TGV. Avec unecontrainte : 30.000 voyageurs par jour. Alors, la métropole a étudié en tout 22scénarios, modélisés comme une carte météo en intégrant non pas destempératures ou des taux d'hygrométrie selon l'altitude, mais le nombre dekilomètres à réaliser et d'ouvrages à construire (tunnels, pont), le potentielde voyageurs, l'impact sur la domanialité. Afin de voir le coût (de 25 M€ du kmà 29 M€) et l'intérêt. A titre d'exemple, le « corridor nord/est », ducôté du lycée Choiseul, ne sert pas à grand-chose compte tenu de la desserte enbus et de la densité de population. Pour le « corridor ouest-Tours »,quatre tracés ont été étudiés dont un par le boulevard Béranger (proximité deshalles/Giraudeau) et l'autre par l'avenue Jean-Royer et les casernesBeaumont-Chauveau. Visiblement, le maire de Tours pousse à la roue pourJean-Royer. Il a même demandé une étude supplémentaire. L'inconvénient est qu'àcause de l'étroitesse de l'axe, il faudrait interdire la circulationautomobile. Pas sûr que les riverains apprécient. Large de 42 mètres, Bérangerdéroulerait… un boulevard au tram. Un tracé par les Tanneurs semble carrémentexclu. Le lieu promis à devenir un axe déterminant est la gare, une bonnealternative à l'engorgement de la place Jean-Jaurès. Pour rejoindre le CHU, destracés passent ensuite par la rue Edouard-Vaillant avec deux difficultés,passer sous la voie ferrée et sur le Cher. Frédéric Augis se reconnaîtinstigateur de cette solution. Pour lui, la métropole doit faire ce que la SNCFne fait pas. « Avec ce nouveau statut, nous sommes co-contractants desgares. » Toutes les précautions oratoires ont été prises pour dire que ladécision n'était pas prise. Avec ces études, les élus ont bien quelques idées,mais le choix sera fait lorsque tous les éléments seront connus, notamment lenombre et le coût des expropriations. L'occasion de rappeler que la premièreligne a coûté 400 M€. Et avec la seconde, il faudra compter 300 M€, sans lefoncier. Alors, autant bien réfléchir. Raphaël Chambriard

.. raw:: html

   </p>
