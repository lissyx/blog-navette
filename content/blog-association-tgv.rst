Blog association TGV
####################
:date: 2015-08-10 06:37
:category: Presse et médias


:slug: blog-association-tgv
:status: published

Extrait du Blog

Quelles sont vos revendications aujourd'hui ?

« On a gagné la bataille de la desserte. Il faut maintenant gagner labataille tarifaire. En dix ans, les tarifs ont augmenté de 32 %. C'est ledouble de l'inflation. Aujourd'hui, cette ligne est la deuxième la plus chèrede France. En moyenne, un abonnement coûte 500 € par an. Nous sommes uneclientèle captive. Les gens ont besoin de ce TGV pour aller travailler et laSNCF le sait, mais il y a un moment où ça coince. Il faut développer uneapproche tarifaire plus flexible avec des offres de dernière minute et unedégressivité des abonnements avec le temps. »

Que pensez-vous du projet de téléphérique entre les gares de Tours etSaint-Pierre ?

« C'est absurde. Une cabine de téléphérique, c'est 40 places maximumalors que nous sommes parfois 500 à attendre le train. Il faut remettre enplace les navettes dédiées sur une ligne dédiée. La CFDT a proposé une solutionintéressante dans ce sens. » à suivre

.. raw:: html

   </p>
