Communiqué de Presse - Rencontre avec Christophe BOUCHET
########################################################
:date: 2019-04-09 08:30
:category: Presse et médias


:slug: communique-de-presse-rencontre-avec-christophe-bouchet
:status: published

Après avoir rencontré les Députés de la circonscription au sujet du projetde navette, ainsi que le Président de la Métropole et le vice-président encharge des Mobilités, une rencontre le 5 avril a eu lieu avec le maire deTours, Christophe Bouchet. Tous les élus partagent le constat sur la piètrequalité du service actuel, et sont intéressés par le projet, et la CFDT arappelé la possibilité offerte par le décret du 10 novembre 2017 de préemptionsur les infrastructures ferroviaires inutilisées. Les élections arriventbientôt, il est certain que le sujet de la desserte des deux gares en ferapartie.

`Communiquéde presse du 8 avril 2019 <{static}/docs/com_presse__Tours_Saint_Pierre_8_avril_2019.pdf>`__

.. raw:: html

   </p>
