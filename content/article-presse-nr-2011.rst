article presse NR 2011
######################
:date: 2015-08-10 06:01
:category: Presse et médias


:slug: article-presse-nr-2011
:status: published

Article de presse NR

Par soucis de faire évoluer la liaison entre les deux gares de Tours etSaint-Pierre-des-Corps, la SNCF a décidé de mettre à la retraite les "petitsgris" qui font la navette pour permettre aux voyageurs de Tours de récupérer lacorrespondance à Saint-Pierre-des-Corps...

On peut dire que les informations données par la SNCF ne sont pas trèsclaires. Apparemment, il a été décidé de remplacer les vielles navettes par desTER qui sont beaucoup plus confortables et silencieux, qui devraient satisfaireles voyageurs.

Néanmoins concernant les cadencements et les horaires des navettes, c'estbeaucoup plus confus car la SNCF annonce que des TER ou trains intercitépourraient faire des haltes à Saint-Pierre et faire office de navette, maisaussi pour éviter le stress de manquer une correspondance, le départ de lanavette aurait lieu au moins une demi heure avant l'horaire de cettecorrespondance TGV !?! Pour rappel, un trajet entre les deux gares dure 5minutes et actuellement, il y a une navette toutes les 10-15 minutes... Donc,si je comprends bien, il a été décidé de rénover la liaison entre les deuxgares mais en échange d'un cadencement beaucoup plus faible qui entraînera doncforcément plus d'attente en gare...

Affaire à suivre donc !

.. raw:: html

   </p>
