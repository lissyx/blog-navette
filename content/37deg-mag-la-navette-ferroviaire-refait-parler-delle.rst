« 37° Mag : La navette ferroviaire refait parler d'elle »
#########################################################
:date: 2016-10-20 15:44
:category: Presse et médias


:slug: 37deg-mag-la-navette-ferroviaire-refait-parler-delle
:status: published

Le magazine en ligne « 37° Mag » revient sur le projet de navettedans `un article mis en ligne le 19 octobre 2016 <http://www.37degres-mag.fr/actualites/la-navette-ferroviaire-entre-tours-et-saint-pierre-des-corps-refait-parler-delle/>`__, et revient notamment sur laquestion du coût avec la différence de chiffrage entre la CFDT et la SNCF. Unecopie de l'article est aussi `disponible <{static}/docs/37degres_-_La_navette_ferroviaire_entre_Tours_et_Saint-Pierre-des-Corps_refait_parler_delle.pdf>`__.

.. raw:: html

   </p>
