La Métropole appuie la solution d'une navette Tours - Saint-Pierre-des-Corps, la balle dans le camps de la SNCF
###############################################################################################################
:date: 2017-09-22 12:27
:category: Presse et médias


:slug: la-metropole-appuie-la-solution-dune-navette-tours-saint-pierre-des-corps-la-balle-dans-le-camps-de-la-sncf
:status: published

Le 12 septembre 2017, la délégation CFDT rencontrait le Vice-présidentdélégué aux mobilités et aux infrastructures du Conseil Métropolitain, FrédéricAugis. Celui-ci confirme l'intérêt fort de la métropole pour le projet denavette ferroviaire porté par la CFDT, et doit de nouveau rencontrer lesinstances de la SNCF, sans qui rien ne pourra se faire sur les emprises nonutilisées depuis 20 ans.

`Communiqué depresse <{static}/docs/communique_de_presse_du_12_sept_2017.pdf>`__

.. raw:: html

   </p>
