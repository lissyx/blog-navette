La navette dans le cadre de Tours Métropole ?
#############################################
:date: 2016-10-17 10:36
:category: Presse et médias


:slug: la-navette-dans-le-cadre-de-tours-metropole
:status: published

Dans le dossier envoyé à l'État pour le projet Tours Métropole tel que\ `rapporté par la Nouvelle République <http://www.lanouvellerepublique.fr/Indre-et-Loire/Actualite/Politique/n/Contenus/Articles/2016/10/13/Tours-Metropole-un-projet-bien-sous-tous-rapports-2869165>`__, l'amélioration de la desserte entreles gares de Tours et Saint-Pierre-des-Corps est mis en avant.

“Viennent ensuite les projets : le développement de l'offre cyclotouriste,« fer de lance de l'excellence touristique », le co-investissement dans leprojet Trousseau 2026, une politique agricole métropolitaine, l'amélioration dela liaison entre les gares de Tours et de Saint-Pierre-des-Corps, ledéploiement des transports en commun avec notamment la « réalisation d'uneseconde ligne de tramway dès 2019 (choix de l'itinéraire d'ici fin 2016)».”

.. raw:: html

   </p>
