Lettre à la Métropole du 30 juin 2018 : la CFDT reçue le 13 juillet
###################################################################
:date: 2018-07-04 12:10
:category: Courriers CFDT


:slug: lettre-a-la-metropole-du-30-juin-2018-la-cfdt-recue-le-13-juillet
:status: published

Suite au `courrier du 30Juin 2018 <{static}/docs/lettre_metrop_30_juin_2018.pdf>`__ à Tours Métropole, son Vice-Président Frédéric Augis a répondufavorablement et recevra la CFDT le vendredi 13 juillet à 10h00.

.. raw:: html

   </p>
