article NR
##########
:date: 2015-08-10 06:06
:category: Presse et médias


:slug: article-nr
:status: published

Tours Saint-Pierre : la navette avance vite - 15/06/201515/06/201505:46

Daniel Bernard, le long de la voie de chemin de fer qui pourrait être dédiéeà la future navette. -

L’idée d’installer des navettes entre les deux terminaux avance bien. Ellespourraient emprunter la cadence du tramway.

L'idée de relier les gares de Saint-Pierre-des-Corps et de Tours, autrementque par le bus, n'est pas nouvelle. Marie-France Beaufils milite depuislongtemps pour une deuxième ligne de tramway… Mais à observer la vitesse àlaquelle avance l'idée, et les prochains parcours qui se dessinent (1) on sedit que le combat de la maire communiste de Saint-Pierre-des-Corps risquel'écueil de la longueur. En revanche, la croisade que mène depuis quelquesannées l'Union départementale CFDT pour imposer l'idée d'une navette pendulaireentre les deux sites paraît de plus en plus crédible. Daniel Bernard,syndicaliste retraité et Claude Garou, secrétaire du comité régional destransports, en sont persuadés après l'entretien que vient de leur accorderSerge Babary, maire de Tours, vice-président de Tour(s) plus et Yves Massot,son adjoint aux transports.

" Supprimer la fracture entre Tours et Saint-Pierre "

« La mairie a trouvé notre projet simple (2), réaliste et à des coûtssemble-t-il acceptable au regard des autres projets débattus par le passé.Notre projet ne nécessite aucune expropriation, simplement des aménagementsd'espaces, des élargissements de plateforme donc de remblais qui peuvent, pourcertaines d'entre elles, s'intégrer dans le projet d'urbanisme lancé par laville pour supprimer la fracture entre les villes de Tours et deSaint-Pierre-des-Corps au niveau de l'autoroute. » Optimisme partagé parYves Massot. « Je confirme la tenue de la réunion et l'intérêt porté àleurs propositions. Ce projet de navettes entre les deux terminaux faisaitpartie de notre programme, comme fait également partie de notre programmel'exploitation de l'étoile ferroviaire. L'idée serait effectivement de créer unflux de navettes qui se calerait sur le rythme du tramway, à savoir entre 6 et10 minutes de temps d'attente en journée et en semaine. » Pour financer ceprojet, estimé par la CFDT entre 10 et 15 M€, le syndicat parle « d'unemprunt qui pourrait être couvert par la mise en place de la Surtaxe localetemporaire, soit une taxe payée par l'usager sur chaque billet vendu quicorrespondrait à 4 % du prix du billet. » « Pour le financement, jene peux pas me prononcer, botte en touche l'adjoint de Tours, il faut d'abordmener les études et en évaluer le coût réel. Ce que je sais, en revanche, c'estque nous aimerions que cette ligne voie le jour avant la fin du mandatactuel. » Une visite sur le terrain est cependant actée entre les deuxparties, avec une présentation du projet à Philippe Briand, président del'agglomération.

(1) Dans un récent entretien accordé à un magazine, Philippe Briandprivilégiait l'idée d'une ligne qui passerait par Saint-Avertin pour rejoindrele site Grandmont et le CHU Trousseau. Puis une autre ligne qui filerait versl'hôpital Bretonneau et la Riche. (2) Le projet est de neutraliser une voiedéjà existante, celle qui longe les hôtels, et qui ne sert à personne. Une voiequi s'arrête aujourd'hui sur un butoir après le pont qui enjambe la rue Jolivetet qu'il faudra donc prolonger jusqu'à Saint-Pierre-des-Corps. Le coût estestimé par la CFDT entre 10 et 15 M€ Jacques Benzakoun

Suivez-nous sur Facebook

.. raw:: html

   </p>
