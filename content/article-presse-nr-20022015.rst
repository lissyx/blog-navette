article Presse NR 20/02/2015
############################
:date: 2015-08-10 06:09
:category: Presse et médias


:slug: article-presse-nr-20022015
:status: published

Comment désengorger la gare de Saint-Pierre-des-Corps ? - 20/02/201505:46

Chaque jour, ce sont entre 10.000 et 12.000 personnes qui se rendent à lagare de Saint-Pierre-des-Corps, beaucoup en voiture. -

La solution pour aérer l’accès à la gare TGV passe par une diminution dunombre de voitures. Plusieurs pistes sont à l’étude, à des degrés différents.Bientôt un parking aérien à proximité ? 80 Gare TGV : la voiture pourprendre le train

::

   Pour résoudre le casse-tête de la gare, des solutions sont souvent avancées. Des plus simples aux plus coûteuses. État des lieux.

::

   La navette SNCF

Depuis 2011, les navettes systématiques entre les deux gares ont étésupprimées. Un dispositif que veut relancer le syndicat CFDT. En attendant, descorrespondances existent. Du côté de la SNCF, on assure que « plus de 60trains par jour, dans les deux sens, permettent de rejoindre Tours etSaint-Pierre-des-Corps et inversement. » Le dispositif mis en place depuis2011 « a permis d'améliorer la ponctualité entre ces deux gares »,poursuit la SNCF. Pour l'avenir, « un groupe de travail sur ces questionsa été mis en place. Il regroupe des représentants de l'agglo de Tours, de laville de Saint-Pierre-des-Corps, du conseil général et de la Région. La SNCFest associée à ce groupe de travail. Les réflexions et les décisions seront, lemoment venu, portées par les collectivités. »

::

   Le tramway

Marie-France Beaufils ne s'en cache pas – « Il faut engager rapidementl'étude de la deuxième ligne de tram » –, affichant ce dossier comme unepriorité. Le tracé est même déjà dessiné sur le papier par la maire. Il passeforcément par les deux gares. Si cette seconde ligne est souhaitée, elle vaprendre du temps. La première a coûté 430 millions d'euros et l'agglomérationfait attention à ses finances.

::

   Le vélo

Si la voiture est le moyen de locomotion favori (56 %), seulement 5,6 % serendent à la gare à vélo, soit environ 625 cyclistes pour les 3.500 voyageursde Tours et Saint-Pierre. Une des raisons : les pistes cyclables, notamment envenant de Tours, ne sont pas complètement satisfaisantes. Les cyclistes fontressortir par exemple que « la piste de la rue des Ateliers est bien, maisil y a beaucoup de sorties de véhicules et des stationnements irréguliers surcelle-ci », affirme un usager. « La piste de l'avenue Jean-Bonnin (dans leprolongement du boulevard Heurteloup) est également assez dangereuse. »L'autre problème, c'est le manque de places de stationnement pour lesdeux-roues. 80 places sont prévues, « mais elles sont à saturation et nonsécurisées. » Que ce soit pour le stationnement ou la circulation, lasénatrice-maire pense à un passage sous les voies ferrées « pourconstruire de nouvelles voies, pistes cyclables ou autres. »

::

   La marche à pied

Selon les chiffres recueillis par la mairie, comme l'explique Daniel Menier,« sur les 1.200 habitants de Saint-Pierre qui fréquentent la gare, environ67 % y vont à pied. Nous savons aussi que la proximité de la gare est unargument important pour s'installer dans la commune. Les aménagements sontaujourd'hui suffisants pour venir à pied. »

insolite

Et la télécabine

Projet farfelu et utopiste ? Lors de la campagne des municipales sur Tours,la liste d'Europe Écologie Les Verts a présenté un projet de télécabine pourrelier les deux gares (via Rochepinard). Plus pratique et plus attractif queles navettes actuelles, la télécabine pourrait séduire des adeptes dumultimodal et leur éviter de prendre la voiture. Si des échos ont parlé d'unregain d'intérêt pour le projet du côté de Tour(s) plus, Jean-Gérard Paumier,vice-président en charge des transports, pense pourtant que « ce n'est pasun projet d'actualité ».

Suivez-nous sur Facebook

.. raw:: html

   </p>
