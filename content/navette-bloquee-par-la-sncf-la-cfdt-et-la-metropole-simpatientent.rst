Navette bloquée par la SNCF : la CFDT et la Métropole s'impatientent
####################################################################
:date: 2018-11-15 10:21
:category: Presse et médias


:slug: navette-bloquee-par-la-sncf-la-cfdt-et-la-metropole-simpatientent
:status: published

Dans le cadre du suivi du dossier de la navette entre les gares de Tours etde Saint-Pierre-des-Corps, la CFDT a été reçue par le vice-président de lamétropole, en charge des mobilités, Frédéric Augis. Alors que la Métropole esttrès intéressée par le projet, la SNCF freine toujours et ne semble pasfavorable à l'amélioration ; devant la détermination elle indique qu'elledonnera sa réponse à la Métropole au cours de l'année 2019. La CFDT a doncdemandé au vice-président qu'une étude soit engagée pour évaluer lespossibilités des différents scénarios.

`Communiqué de presse du 12novembre 2018 <{static}/docs/com_presse_du_12_nov_18.pdf>`__

.. raw:: html

   </p>
