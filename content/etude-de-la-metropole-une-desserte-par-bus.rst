Étude de la Métropole : une desserte par bus ?
##############################################
:date: 2019-01-10 16:54
:category: Presse et médias


:slug: etude-de-la-metropole-une-desserte-par-bus
:status: published

Suite à l'article de La Nouvelle République, un lecteur, Michel Deguet,\ `a fait part de ses observationset craintes <{static}/docs/NR_du_26.12.18_-__Deguet.pdf>`__ quant à la seconde étude récemment lancée par la Métropole, quipourrait signifier la mort du projet de desserte ferroviaire et la préférenced'une option par bus, rejoignant ainsi la vigilance de la CFDT sur l'avenir duprojet.

.. raw:: html

   </p>
