courrier de la mairie 2 aout 2015
#################################
:date: 2015-08-10 06:52
:category: Courriers Institutions


:slug: courrier-de-la-mairie-2-aout-2015
:status: published

Bonjour,

Pour faire suite au rendez-vous avec M. BABARY en juin dernier, je vousinforme que celui-ci organise à nouveau une réunion à laquelle vous êtesconvié, en présence de M. BRIAND, Président de l’Agglo et de M. PAUMIER,Vice-Président en charge du Transport,

Le samedi 5 septembre à 10 h (lieu à préciser).

Je vous remercie de me confirmer votre présence.

Meilleures salutations.

Sylvie ROBERT

Assistante de M. le Maire

.. raw:: html

   </p>
