Dernières nouvelles au sujet du dossier « Navettes Tours St-Pierre »
####################################################################
:date: 2018-07-23 07:57
:category: Courriers Institutions


:slug: dernieres-nouvelles-au-sujet-du-dossier-navettes-tours-st-pierre
:status: published

Pour faire suite à notre courrier, le Vice-Président de la Métropole F.AUGIS, nous avait convié à une rencontre prévue le vendredi 13 juillet. Laveille il nous informe qu’il reporte cette rencontre en raison notamment del’attente d’une date de rencontre avec Stéphane VOLANT qui est le secrétairegénéral de la SNCF (l’un des bras droit de G.Pepy). Dans ces conditions, nousserons de nouveau convié à une rencontre avec la Métropole dès que F. AUGISaura débattu du sujet avec le représentant de la SNCF Stéphane VOLANT.

Ci-joint un courrier de `lettre-briand-16-juillet-2018P.BRIAND Président de Tours Métropole en date du 16 juillet <{static}/docs/lettre_Briand_16_juillet_2018.pdf>`__.

.. raw:: html

   </p>
