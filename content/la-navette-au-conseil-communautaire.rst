La navette au conseil communautaire
###################################
:date: 2016-10-18 11:53
:category: Presse et médias


:slug: la-navette-au-conseil-communautaire
:status: published

L'agglomération a abordée la question des transports au cours de l'une desdélibérations le 17 octobre 2016, au travers d'une question de Vincent Tison,plaidant pour une étude indépendante sur le coût annoncé de 100M€ par la SNCF,comme `rapporté par La Nouvelle République <http://www.lanouvellerepublique.fr/Indre-et-Loire/Communes/Tours/n/Contenus/Articles/2016/10/18/Les-gares-a-relier-par-la-terre-ou-l-air-2874255>`__. Par ailleurs, Frédéric Augis souhaitevoir le téléphérique implanté à Brest, pour pouvoir décider (ou non) de cettehypothèse.

.. raw:: html

   </p>
