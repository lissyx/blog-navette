NR
##
:date: 2017-04-27 15:25
:category: Presse et médias


:slug: nr
:status: published

Deuxième ligne de tram : pas de temps à perdre ! 04/04/2015

Pour l’ADTT et certains élus, la réflexion sur la création d’une deuxièmeligne de tramway doit être lancée au plus vite. Objectif : une ouverture d’ici2021. A l'invitation de l'Association pour le développement du transportcollectif en Touraine (ADTT), une cinquantaine de personnes ont participé,jeudi soir, aux halles, à un débat sur « l'urgence » d'une deuxièmeligne de tramway. Une façon pour l'association, qui propose plusieurs tracés(extension de la ligne A et/ou création de la B, voir infographie), de pousserà la roue pour que la communauté d'agglomération engage au plus vite laréflexion. « Si aucune orientation n'est prise fin 2015, il n'y aura pasd'ouverture de ligne avant 2021, a insisté Jean-François Troin, président del'ADTT. Actuellement, on a l'impression d'une certaine somnolence : Tours" digère " son tramway. » Décision à l'automne ? Pour alimenter le débatce jeudi, l'association a rencontré ces derniers mois les équipes municipalesde six communes potentiellement concernées par une deuxième ligne (Tours,Saint-Pierre-des-Corps, Saint-Avertin, La Riche, Saint-Cyr-sur-Loire etChambray-lès-Tours). De ces entretiens, il est notamment ressorti le souhait,d'une partie des villes, de voir une liaison par tramway ou bus à haut niveaude service (BHNS) mise en place entre les CHU Trousseau et Bretonneau, ainsiqu'une desserte efficace entre les gares de Tours et Saint-Pierre-des-Corps.« Le tramway apporterait une réponse très cadencée entre les deux gares »,a estimé jeudi soir la maire PCF de Saint-Pierre-des-Corps, Marie-FranceBeaufils, qui défend également un passage par le quartier de la Rabaterie. Lapériode est même « favorable », selon l'élue, pour que Tour(s)plus serelance dans un projet tram : « Le coût financier des prêts estaujourd'hui faible. » Et l'Europe prête a priori à mettre au pot dans lecadre d'un appel à projets. Emmanuel Denis (EELV), conseiller municipal deTours et conseiller communautaire, considère lui aussi qu'il est indispensablede s'engager dans un nouveau projet tram sur l'agglo : « C'est un sujetsanitaire et environnemental, il faut choisir très vite un tracé. » Unpoint de vue partagé par le député socialiste Jean-Patrick Gille : « Il nefaut pas tarder. » Mais pour l'adjoint à la circulation de Tours, YvesMassot (UMP), il est impératif, avant de s'engager, « de faire un pointfinancier pour voir où on en est ». Le président de Tour(s)plus, PhilippeBriand, n'est en tout cas pas hostile à une deuxième ligne de tramway : lundi,lors du conseil communautaire, il a laissé entendre qu'une décision pourraitintervenir à l'automne. Johan Guillermin

.. raw:: html

   </p>
