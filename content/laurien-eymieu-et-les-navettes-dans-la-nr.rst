laurien Eymieu et les navettes dans la NR
#########################################
:date: 2015-09-20 11:52
:category: Presse et médias


:slug: laurien-eymieu-et-les-navettes-dans-la-nr
:status: published

La fin des navettes Tours/Saint-Pierre ? 09/12/2011 05:23

« La circulation des navettes dans le noeud ferroviaireTours-Saint-Pierre (près de 60 trajets par jour) est un facteur de fragilité,assure Laurence Eymieu. Le plus possible, nous avons cherché à assurer lescorrespondances entre les deux gares sur des trains TER ou Intercités existantspour désengorger le système. A partir du 11 décembre, la moitié descorrespondances entre les deux gares sera assurée par des trains commerciauxexistants et par des trains modernes et confortables type TER mis à dispositiondes voyageurs pour compléter l'offre. Les vieilles navettes non accessiblesvont disparaître du paysage. » Reste que les usagers craignent des délaisd'attente rallongés. « Nous avons été attentifs à ce que le délai decorrespondance soit de 10 minutes environ. Les voyageurs en correspondance nesubiront donc pas de rallongement de leur trajet. Nous avons des règlesprécises en matière de gestion des correspondances, et d'autant plus dans unsystème cadencé. »

.. raw:: html

   </p>
