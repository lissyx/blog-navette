navettes NR du 25 sept 2015
###########################
:date: 2015-09-25 16:08
:category: Presse et médias


:slug: navettes-nr-du-25-sept-2015
:status: published

Tours, Saint-Pierre-des-Corps - Transports Navette Tours Saint-Pierre :un projet sans concurrence ? 25/09/2015 05:46 Nombre de commentairesréagir(3)

Ce soir, lors d’une réunion publique, la CFDT présentera son projet denavettes entre Tours et Saint-Pierre.

Après avoir hanté les couloirs des différents pouvoirs politiques – ville deTours, Tour(s)plus, région Centre ou encore Réseau Ferré de France, SNCF (1) –,l'Union départementale CFDT d'Indre-et-Loire va présenter son projet denavettes entre Tours et Saint-Pierre-des-Corps aux usagers. Cette rencontre alieu ce vendredi, à 20 h, à l'hôtel de ville de Tours, à l'initiative del'association des usagers TGV et de la mairie de Tours (2).

En attendant, l'infatigable ambassadeur de ce projet, Daniel Bernard,revient en détail sur un plan qui, de son point de vue, permettrait undémarrage assez rapide des travaux sans que l'impact foncier ne gêne personne.« C'est une ligne de 2,3 km qui partirait à la hauteur de la rueMarcel-Tribut et qui emprunterait, dans sa première partie, la voie A1 quin'est aujourd'hui, utilisée par personne (la plus à l'est de la gare). Cettevoie s'arrête à la hauteur de la rue Jolivet, à l'endroit même ou passe le pontSNCF. Il faudrait alors élargir la plateforme et le tablier du pont. Le pontfranchi, on longe les préfabriqués et on passe sur le parking sous l'autoroute.Là, il nous faut une emprise d'environ 3 mètres de largueur. Le terrain existe,mais il n'y a pas de voies. Il faudrait donc en créer une voie pour rejoindrele parco train de Saint-Pierre-des-Corps, en passant à l'est des trois piles depont. La plupart des terrains appartiennent à la SNCF ou à l'agglomération,aucune enquête publique, aucune expropriation à faire et nous avons déjà unkilomètre de voie existant. » La CFDT a même envisagé l'aménagement d'unespace permettant le croisement de deux lignes, à la hauteur du parking desPeupliers, là où existe un vieux bâtiment de bois qui appartient à la SNCF.

Pas d'expropriation

Dernier argument de la CFDT : en termes de coût, l'aménagement qui nenécessiterait aucun déplacement de réseau est chiffré entre 10 et 15 M€ maximumquand la création d'un kilomètre de tramway se chiffre en moyenne autour de 20M€ (22,7 M€ à Tours, 31 M€ à Orléans). Restera ensuite à traiter le problèmedes véhicules. Recycler d'anciennes rames ou acheter du matériel neuf quiredonnera du style et de l'attrait à cette liaison très particulière qui relieTours et Saint-Pierre-des-Corps. Cinq minutes, montre en main.

(1) Si la plupart des interlocuteurs rencontrés, notamment la ville de Tourset Tour(s) plus se disent intéressés par le projet, Yvon Borri le directeurrégional de la SNCF ne souhaite pas neutraliser la partie est des voies carelle « empêcherait tout développement ultérieur, étant qu'elle sesituerait en lieu et place d'une 3e voie potentielle pouvant assurer lacirculation de trains supplémentaires. » De son côté, Réseau Ferré deFrance n'est pas hostile au projet mais pointe du doigt des« investissements potentiellement élevés ». (2) Lors de cette réunion, ilsera également question des impacts de la LGV 2017 sur les horaires, lesfréquences. Enfin sera posé le problème des parkings aux abords des gares.Jacques Benzakoun

.. raw:: html

   </p>
