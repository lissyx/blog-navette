article presse NR 07/08/2014
############################
:date: 2015-08-10 06:19
:category: Presse et médias


:slug: article-presse-nr-07082014
:status: published

Navettes SNCF Tours - Saint-Pierre : la CFDT interpelle les élus -07/08/2014 05:22

A la veille des vacances, l'Union départementale de la CFDT a écrit aux élusde l'agglomération de Tours, du Département et de la Région pour lesinterpeller sur les problèmes de correspondance ferroviaire entre les deuxgares de Tours et Saint-Pierre-des-Corps. En préambule, le syndicat rappellequ'il faut souvent compter 15 à 20 minutes supplémentaires pour effectuer letrajet depuis la disparition des anciennes navettes SNCF. A la fin de l'annéedernière, la CFDT a présenté un projet alternatif qui permettrait de mettre encirculation de nouvelles navettes dédiées « en utilisant une grande partiede l'infrastructure existante… sans expropriations ». Moyennant « un coûtraisonnable », les principaux investissements seraient liés à l'élargissementdu pont de la rue Edouard-Vaillant et l'aménagement d'une voie nouvelle auxabords de la gare de Saint-Pierre. Le syndicat a transmis ce « projetréaliste » aux élus en précisant qu'il ne devait pas s'opposer à laréalisation d'une deuxième ligne de tramway vers Saint-Pierre-des-Corps et LaVille-aux-Dames. Dans son courrier, il précise que Réseau ferré de France (RFF)– propriétaire des emprises – s'est déclaré « intéressé par le principed'une étude ».

.. raw:: html

   </p>
