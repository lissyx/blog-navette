NR du 25-11-2017 : Tours St-Pierre, TRAM ou Navette ,
#####################################################
:date: 2017-11-26 11:28
:category: Presse et médias


:slug: nr-du-25-11-2017-tours-st-pierre-tram-ou-navette
:status: published

Tours – Saint-Pierre : Tram ou navette ?

Alors que Marie-France Beaufils réclame toujours son tramway, Ia CFDTpoursuit sa croisade pour une navette sur rails entre Tours etSaint-Pierre.

Tram, ne lâchons rien. C'est par ces quelques mots très explicites queMarie-France Beaufils titre un éditorial dans sa dernière revue municipale. Auxmaires de l'agglomération, notamment ceux de I ‘Ouest, qui se réjouissent devoir pointer le nez de la deuxième ligne, Mme Beaufils rappelle qu'il y aégalement une vie à l'est de Tours. » Le tramway est un véritable outild'aménagement du territoire » rappelle-telle, reprise en écho par uncollectif corpopétrussien qui collectionne déjà 2.000 signatures à sonactif.

En parallèle, et sans que sa position ne vienne percuter frontalement cellede Marie-France Beaufils (encore que...), Ia CFDT cheminote et son comité depilotage rappellent qu'il y a urgence à proposer un vrai cadencement denavettes entre Tours et Saint-Pierre-des- Corps. « Un tram vers Saint-Pierre, pourquoi pas, soupire Daniel Bernard, mais dans combien de tempsarrivera-t-il ? Et encore s'il arrive un jour ! Notre propositiond'utiliser une voie inoccupée le long de la rue Edouard-Vaillant jusqu'à lagare de Saint- Pierre-des-Corps est nettement plus simple, exige moins detravaux d’infrastructure et présente l’avantage d'être beaucoup moins onéreuse.Là où nous chiffrons notre projet entre 12 et 15 M€ pour 2,7 km de voie (avecaménagement sous le pont autoroutier et élargissement du tamis du pont quienjambe la rue Jolivet), il faudrait 3,5 km de site protégé pour le tramway à25 M€ le kilomètre. »

Dialogue renoué avec la SNCF. Et le syndicaliste de devancer les critiquesqui pourraient accompagner sa position : « Si le but déclaré estd'établir une liaison rapide entre les deux gares pour désengorger celle deSaint- Pierre-des-Corps, alors il n'y a pas photo. En revanche, s’il s'agitd'un projet urbanistique pour mettre en valeur des quartiers deSaint-Pierre-des Corps, c'est un autre sujet. »

Daniel Bernard se dit aujourd'hui plus confiant qu'hier. « Au sein dela Métropole, notre proposition a reçu un bon accueil. Maintenant, il fautconvaincre la direction régionale de la SNCF et la direction réseau qui ontchangé de patron. Les premiers contacts sont meilleurs que par le passé et desdiscussions ont pu être reprises. Il faut juste rappeler que la voie qui seraitutile... est inutilisée depuis une bonne vingtaine d'années ! »

« Notre proposition est plus simple et beaucoup moins onéreuse.>> Jacques Benzakoun

.. raw:: html

   </p>
