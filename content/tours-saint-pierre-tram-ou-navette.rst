Tours - Saint-Pierre : tram ou navette ?
########################################
:date: 2017-11-27 14:18
:category: Presse et médias


:slug: tours-saint-pierre-tram-ou-navette
:status: published

Dans `la Nouvelle République du 25 novembre 2017 <https://www.lanouvellerepublique.fr/tours/tours-saint-pierre-tram-ou-navette>`__, la question de ladesserte entre Tours et Saint-Pierre-des-Corps est présentée, en rappelant lesdifférences dans les objectifs des deux projets, et notamment le coût aukilomètre plus élevé du tram, ainsi que l'objectif de la desserte :améliorer la connexion entre les deux gares, et désengorgerSaint-Pierre-des-Corps, ou projet de réaménagement urbain pour mieux mettre envaleur Saint-Pierre-des-Corps ?

.. raw:: html

   </p>
