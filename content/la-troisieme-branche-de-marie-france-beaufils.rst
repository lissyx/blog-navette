La « Troisième branche » de Marie-France Beaufils
#################################################
:date: 2017-10-18 14:11
:category: Presse et médias


:slug: la-troisieme-branche-de-marie-france-beaufils
:status: published

Un amendement a été proposé par la maire de Saint-Pierre-des-Corps,proposant de connecter une « troisième » branche de la deuxième lignede tramway entre les gares de Tours et de Saint-Pierre-des-Corps ;rejointe par sa première adjointe qui souligne l'importance de desservir lesgares plutôt que l'aéroport, l'Est de l'agglomération étant laissée pour comptepour le moment. L'adjointe de Chambray-lès-Tours rajoute que ce sont deséléments qui figuraient dans le dossier de la métropole de Tours. L'amendementsera rejeté, mais Philippe Briand et Frédéric Augis semblent partager l'avis del'importance de la desserte efficace entre les deux gares. `L'article complet de la Nouvelle République <{static}/docs/tram-beaufils.pdf>`__

.. raw:: html

   </p>
