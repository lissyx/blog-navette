Nouvelle republique du 02/10/2017, ligne 2 du Tram
##################################################
:date: 2017-10-02 08:28
:category: Presse et médias


:slug: nouvelle-republique-du-02102017-ligne-2-du-tram
:status: published

Tramway La Riche/Chambray : des gagnants, un perdant

::

   NR – d’Indre et Loire02/10/2017

Les élus métropolitains ont confirmé leur volonté de relier les hôpitaux viala deuxième ligne de tramway, tant pis pour Saint-Pierre… pour l’instant.

::

   C'est donc acté, ou presque, la deuxième ligne de tramway de la métropole Tours Val de Loire va relier les deux centres hospitaliers, Trousseau et Bretonneau, à l'horizon 2024. Mais pas seulement…

Les études ont été poussées jusqu'au périphérique des deux côtés.« L'idée est de créer de grands parkings relais en entrée de ville. Nousavons réfléchi sur deux demi-lignes qui se rejoignent », explique PhilippeBriand, le président. Beaucoup d'incertitudes demeurent encore sur le tracé :Béranger ou Jean-Royer ? Grammont tout le long ou passage sur la premièreligne ? Forcément une bonne nouvelle pour les nouvelles communes reliées par letramway.Christian Gatard, maire de Chambray,indique : « J'ai toujoursdéfendu ce tracé, pas pour ma commune mais pour l'avenir du CHU. Notre centrehospitalier est actuellement le cinquième au niveau national, mais laconcurrence existe, cette ligne de tramway est un atout supplémentaire pour sondéveloppement. » En revanche, pour Marie-France Beaufils, maire deSaint-Pierre- des-Corps, commune une nouvelle fois oubliée, la déception esttoujours là : « Nous avons mené des études pour un passage à la Rabâterie,par la gare, le parc des expositions et les Atlantes, elles n'ont pas étéassociées aux études sur la deuxième ligne dans un premier temps, c'estdommage. La question de la desserte de la gare est essentielle. Nous allonscontinuer à discuter, nous n'avons pas dit notre dernier mot. » PhilippeBriand ne compte pas lui aussi en rester là : « Nous devons aller plusloin, réfléchir à long terme, et lancer les études pour les grands projets,pour être prêt le moment venu pour bénéficier de financements publics : unetroisième ligne de tramway, l'aréna, l'aéroport… » Quant à la situation dela gare TGV de Saint-Pierre, « Il faut travailler avec la SNCF pour savoirce qu'elle compte faire dans l'avenir entre Saint-Pierre etTours-Centre. » La liaison vers l'aéroport devrait, elle, rapidement voirle jour, ajoute le président : « Il ne reste que 700 mètres sans emprisefoncière. Cela se fait en parallèle avec la réflexion sur l'avenir dusite. »

::

   Lire également en p. 10.

et la troisième ligne ? La deuxième ligne de tramway a peine lancé, PhilippeBriand évoque déjà la possibilité d'en réaliser dans la foulée une troisième.Elle serait sans surprise orientée vers la partie « est » del'agglomération, en traversant Saint-Pierre des Corps. « Elle pourraitégalement passer dans les hauts de Saint-Cyr pour desservir la ville deSaint-Pierre et la gare. L'idée est la même : capter les entrées dansl'agglomération. » Aucun calendrier n'est annoncé : « mais nousdevons étudier rapidement cette possibilité », assure le président. SamuelMagnant Suivez-nous sur Facebook

.. raw:: html

   </p>
