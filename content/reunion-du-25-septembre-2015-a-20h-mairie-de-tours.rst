Réunion du 25 septembre 2015 à 20h, mairie de Tours
###################################################
:date: 2015-09-27 10:40
:category: Projet


:slug: reunion-du-25-septembre-2015-a-20h-mairie-de-tours
:status: published

Le 25 septembre 2015 se tenait à 20h en mairie de Tours une réunion publiquesur l'avenir de la desserte TGV Tours-Paris.

Les points à l'ordre du jour étaient :

-  Amélioration du temps de trajets sur les trains Tours-Paris, pour revenir àenviron une heure (contre 1h15 en moyenne actuellement, voire 1h25 sur certainstrains)
-  Amélioration de la ponctualité, avec seulement 79% de TGV à l'heure sur lepremier semestre 2015
-  Adaptation des horaires pour mieux coïncider avec les besoins des usagers(TGVs le soir, etc.)
-  Amélioration de la desserte de Massy TGV et de l'Aéroport RoissyCharles-de-Gaulle
-  Retour des navettes entre les gares de Tours et Saint-Pierre-des-Corps
-  Amélioration de l'accès au gares : places de parkings, co-voituragesentre TGVistes, garages à vélos

`Tract-reunion-25-septembre-2015 <{static}/docs/LETTER_TGV_2015_Tours__3_.pdf>`__

.. raw:: html

   </p>
