article journal Transport Rail mars 2015
########################################
:date: 2015-08-11 08:43
:category: Presse et médias


:slug: article-journal-transport-rail-mars-2015
:status: published

14 novembre 2014 Tours : vers un retour des navettes

En décembre 2011, la mise en place du cadencement avait été l'occasion pourla SNCF de supprimer les navettes Tours - Saint Pierre des Corps et Orléans -Les Aubrais. L'accès au centre-ville de Tours se fait depuis en utilisant desTER des axes Orléans - Tours et Bourges - Tours, et quelques navettesrésiduelles assurées par des matériels TER, le plus souvent des X73500 de 80places seulement, un peu juste lorsqu'arrive des TGV en unité multiple.

Si le TGV met Saint Pierre des Corps à 55 minutes de Paris, il fautdésormais entre 15 et 20 minutes pour effectuer le trajet final de 2 km jusquesous la verrière de la gare de Tours.

310813_A325saint-pierre-des-corps

Saint Pierre des Corps - 31 août 2013 - La rapidité du TGV est en partieconsommée par la perte de temps créée par la disparition des navettestourangelles. La rame 325, celle des records de vitesse de 1989 (482,4 km/h) etde 1990 (515,3 km/h), laisse ses voyageurs attendre un TER déjà bien remplipour rejoindre Tours. © transportrail

Le syndicat CFDT a proposé de réexaminer le plan de voies et d'essayer derecréer un système de navettes en utilisant une voie qui se termine en impasseà moins d'un kilomètre des quais de Saint Pierre des Corps. Sa prolongationimpliquerait d'élargir le tablier d'un pont routier et de passer entre lespiles du pont de l'autoroute A10.

Cependant, jusqu'en 2011, le fonctionnement des navettes sur les voiesexistantes était relativement correct et des aménagements avaient été réalisésdans le cadre du CPER 2007-2014 pour améliorer les conditions d'entrée en garede Tours. Il semblerait donc à privilégier le retour de navettes mais sur lesinfrastructures existantes. La question est plutôt celle du matériel roulant.Les Z5300 étaient vétustes et ont donc été logiquement réformées, et lesmatériels TER ne sont pas tous adaptés pour effectuer un échange rapide desvoyageurs.

Posté par ortferroviaire à 10:35 - Centre - Commentaires `7 <7>`__ - Permalien `# <#>`__ Tags : navette Tours -Saint Pierre des Corps

.. raw:: html

   </p>
