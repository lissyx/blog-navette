Suppressions de guichets de vente en gare de Saint-Pierre-des-Corps
###################################################################
:date: 2016-10-12 10:29
:category: Presse et médias


:slug: suppressions-de-guichets-de-vente-en-gare-de-saint-pierre-des-corps
:status: published

La SNCF annonce de grosses restructurations à partir de 2017 avec,notamment, en février, la fermeture des guichets de vente aux voyageurs, quiresteraient éventuellement ouverts « en période de forte affluences ».D'autres réorganisations impactent le contrôle à bord des TER et les ventesTER.

Plus de détails sur La Nouvelle République : `Gros changements à la gare de Saint-Pierre-des-Corps à partir de 2017 <http://www.lanouvellerepublique.fr/Indre-et-Loire/Actualite/Economie-social/n/Contenus/Articles/2016/10/12/Gare-TGV-de-Saint-Pierre-des-Corps-de-gros-changements-a-venir-2867951>`__

.. raw:: html

   </p>
