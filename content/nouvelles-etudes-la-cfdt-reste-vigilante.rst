Nouvelles études : la CFDT reste vigilante
##########################################
:date: 2019-01-10 16:47
:category: Presse et médias


:slug: nouvelles-etudes-la-cfdt-reste-vigilante
:status: published

Suite à la publication de `La NouvelleRépublique du 22 décembre 2018 <{static}/docs/NR_du_22.12.18.pdf>`__, la CFDT s'inquiète de la deuxième étudelancée par la Métropole d'une desserte entre les 2 gares de St-Pierre et Tourspar bus et qui desservirait également une partie de la la ville de St-Pierrepar un nouveau parcours (hors des rails). Cette étude de la Métropole, est-ellela fin du projet porté par la CFDT ?

.. raw:: html

   </p>
