article NR du 6 septembre 2015
##############################
:date: 2015-09-06 05:58
:category: Presse et médias


:slug: article-nr-du-6-septembre-2015
:status: published

Tour(s)plus et la CFDT d'une même voie 06/09/2015 05:27

Daniel Bernard, de la CFDT, est confiant dans l'avenir de son projet denavette SNCF entre Tours et Saint-Pierre-des-Corps. - Daniel Bernard, de laCFDT, est confiant dans l'avenir de son projet de navette SNCF entre Tours etSaint-Pierre-des-Corps.

Une fois n'est pas coutume : des syndicalistes de la CFDT sont en train deconvertir à leur thèse la majorité droitière de Tour(s)plus. Au point qu'il sedit dans les coulisses que, si la droite ravissait, à la fin de l'année, laprésidence de la Région à la gauche, le dossier avancerait encore plus vite… Dequoi s'agit-il ? Depuis plusieurs mois déjà, la section CFDT de la SNCF avec, àsa tête, Daniel Bernard, planche sur la mise en place d'une navette ferroviaireentre Tours et Saint-Pierre-des-Corps. Un outil de liaison qui aurait le mérited'utiliser des voies existantes et qui coûterait, à priori, sans doute moinscher que l'aménagement d'une nouvelle ligne de tram réclamée par Marie-FranceBeaufils, maire de Saint-Pierre-des-Corps. Pour Daniel Bernard, reçu samedi parPhilippe Briand, président de l'agglo en compagnie de Serge Babary, 1ervice-président-maire de Tours, Jean-Gérard Paumier, maire de Saint-Avertin etYves Massot, maire adjoint de Tours chargé de la circulation, la somme destravaux d'infrastructure pourrait osciller entre 10 et 15 M€. Daniel Bernardétait accompagné de Guy Sionneau, secrétaire de l'UD-CFDT, et de Claude Garou,secrétaire du comité régional des transports. « La réunion a duré près dedeux heures et elle s'est très bien passée, raconte Daniel Bernard. Les élussemblent convaincus de l'utilité de cette liaison qu'on pourrait faire passersur une voie, aujourd'hui inoccupée, le long de la rue Édouard-Vaillant.« Il y aurait, bien sûr, des travaux d'infrastructure avec notammentl'élargissement du tablier du pont de la rue Jolivet. Si ce projet voyait lejour, la fréquence pourrait être d'une navette toutes les dix minutes enpériode de pointe. » Côté mairie de Tours, le vœu avancé récemment dansnos colonnes par Yves Massot était de créer cette voie avant la fin de l'actuelmandat municipal, mais avec encore beaucoup de conditionnels. JacquesBenzakoun

.. raw:: html

   </p>
