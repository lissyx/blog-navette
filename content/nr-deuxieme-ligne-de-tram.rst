NR - deuxieme ligne de tram
###########################
:date: 2017-04-27 15:23
:category: Presse et médias


:slug: nr-deuxieme-ligne-de-tram
:status: published

Deuxième ligne de tram : les priorités de l'ADTT 27/04/2017 05:36Nombre de commentaires réagir(1) Envoyer par mail Imprimer L'Association pourle développement du transport collectif en Touraine (ADTT) attend de connaîtrele tracé de la future deuxième ligne. - L'Association pour le développement dutransport collectif en Touraine (ADTT) attend de connaître le tracé de lafuture deuxième ligne.

Alors que l’on ne connaît pas encore le tracé de la 2 e ligne, l’associationpour le développement du transport collectif en Touraine détaille sespropositions.

Comme nombre de Tourangeaux, l'Association pour le développement dutransport collectif en Touraine (ADTT) attend de connaître le tracé de lafuture deuxième ligne.

Si l'association regrette le délai entre la mise en service de la ligne A(2013) et le lancement attendu pour la ligne B (2023-2024 ?), elle espère quele processus de développement du réseau s'accélérera dans les prochaines annéespour démontrer que Tours a « réellement les moyens (de transports) d'unemétropole ».

::

   > La liaison Chambray - Verdun. « C'est la priorité », selon Jean-François Troin, président de l'ADTT. L'association préconise un passage par les Fontaines, et une remontée en suivant l'Alouette. Dans ce cas, la ligne ne passerait pas au plus près des sites scolaires et universitaires du bois de Grammont ? : « Ce n'est pas trop gênant car la distance pour rejoindre la ligne de tram reste acceptable. »

La ligne se terminera dans l'enceinte de Trousseau mais l'ADTT propose unprolongement jusqu'à La Papoterie avec la création d'un parking relais.L'association préconise également de revoir le tracé de la ligne de bus à hautniveau de service 2. « Actuellement, elle dessert Trousseau ; demain, ellepourrait se terminer à la clinique du Vinci, à Chambray. »

::

   > Desservir Saint-Pierre. Dans un souci d'« équité sociale », l'ADTT estime que le tram doit passer par Saint-Pierre en empruntant le tronçon gare de Tours - Heurteloup - avenue Jean-Moulin - Rabaterie.

Selon l'ADTT, un prolongement jusqu'à la gare TGV rallongera le parcours et« nuira » à la fréquentation des voyageurs souhaitant rejoindre lesdeux gares. Pour rejoindre les deux gares, le projet de navette sur voie dédiéeportée par la CFDT a la préférence de l'ADTT. Avec l'antenne de Trousseau, cepassage à Saint-Pierre pourrait créer une vraie deuxième ligne entre Chambrayet Saint-Pierre, « tel que le Sitcat (1) le préconisait en 2007 »,rappelle Jean-François Troin.

::

   > Deux options pour Bretonneau. Dans un troisième temps, le développement du réseau pourrait se faire vers l'ouest, vers Bretonneau, puis La Riche. « Entre un passage par le boulevard Jean-Royer ou les Casernes, ou le boulevard Béranger, deux options restent possibles. » Cette liaison permettrait de créer une troisième ligne est-ouest (Saint-Pierre - Chambray). « Nous aurions alors un véritable réseau avec trois lignes et cinq terminus (2). »

::

   (1) Syndicat intercommunal des transports de la communauté d'agglomération de Tours, dissous en janvier 2014. (2) Vaucanson, Jean-Monnet, Trousseau, Rabaterie, Bretonneau - La Riche.

L'ADTT dispose d'un site internet (adttouraine.com) et d'une page facebook(usagersbustramtours). L'adhésion coûte 15 euros par an. Contact :jf-troin@orange.fr

la phrase

" Tours Métropole Val de Loire s'engagera activement dans ce projet (demodernisation du CHU Trousseau ndlr) notamment par la construction d'unenouvelle ligne de tramway qui reliera les deux sites hospitaliers et desserviraen son coeur le pôle hospitalier Trousseau. "

La Métropole se félicite de l'engagement financier (75 millions d'euros) del'État dans le projet du nouveau Trousseau. Et en profite pour (re) préciserles sites prioritaires de la 2e ligne de tram : Trousseau et Bretonneau. Cela ale mérite d'être clair.

Suivez-nous sur Facebook

.. raw:: html

   </p>
