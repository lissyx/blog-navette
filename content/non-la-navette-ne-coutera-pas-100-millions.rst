Non, la navette ne coûtera pas 100 millions
###########################################
:date: 2016-10-17 10:46
:category: Courriers CFDT


:slug: non-la-navette-ne-coutera-pas-100-millions
:status: published

Suite à l'organisation d'une conférence de presse en gare deSaint-Pierre-des-Corps au sujet de la fermeture des guichets SNCF, le `directeur régional estime le coût d'infrastructure de la navette à 100M€ <http://www.lanouvellerepublique.fr/Indre-et-Loire/Actualite/Politique/n/Contenus/Articles/2016/10/16/Une-station-multimodale-cote-Sernam-2872016>`__.La CFDT réagit par une communication à la communauté d'agglomérationTour(s)Plus détaillant les coûts : `lettre à l'agglo du 16 octobre2016 <{static}/docs/lettre_a_l_agglo_du__16_oct_2016.pdf>`__

.. raw:: html

   </p>
