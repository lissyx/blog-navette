courrier UD CFDT 13 juillet 2017
################################
:date: 2017-07-13 07:35
:category: Courriers CFDT


:slug: courrier-ud-cfdt-13-juillet-2017
:status: published

Union Départementale CFDT d’Indre et Loire « La Camusière » 18 ruede l’oiselet 37550 Saint Avertin

Tél : 02 47 36 58 58 Fax : 02 47 36 58 51 Email :indre-loire@centre.cfdt .fr

Tours le 12 juillet 2017

Nouvelle République 37 Monsieur le Rédacteur en Chef

Communiqué de Presse UD-CFDT37 Objet : liaison entre les gares de Tourset de St-Pierre des Corps

Monsieur,

L’UD-CFDT37 sera de nouveau reçu à Tours-Métropole le 12 septembre 2017 à 11h00 pour faire suite à son courrier du 8 juin 2017, adressé aux représentantsde Tours-Métropole dans lequel elle interpellait les élus sur les informationsparues dans la presse sur les projets de tracés de la 2 éme ligne de tram, àsavoir ;

Projet 2A) au-delà du parcours complexe qui passerait par le boulevardHeurteloup, puis la Rabatterie, la mairie, puis la gare de St-Pierre des Corps,il serait intéressant de connaitre dans ces conditions le temps de trajet pourrelier dans ce contexte la gare de Tours à la Gare de St-Pierre des Corps.Comme l’a d’ailleurs souligné l’ADTT, ce tracé ne correspondra pas aux attentesdes voyageurs.

Projet 2B) ce tracé correspond exactement à l’itinéraire de notre projet,sauf que celui de la CFDT ne passe pas rue Edouard Vaillant, mais empreinte unvoie ferroviaire existante (longeant la rue Edouard Vaillant), donc sansemprise sur le réseau routier. De plus, pourquoi vouloir faire passer ce tracépar la rue des ateliers, plutôt que de longer les voies ferrées existante commenous l’avons suggéré, y compris en intégrant le projet de l’agence d’Urbanismedans le cadre du concours « passage », comme nous l’avons déjà développéprécédemment avec une possibilité d’un point d’arrêt à cet endroit. Projet 2C)Là encore, il s’agit d’un parcours complexe et très couteux passage sous lesvoies SNCF) et un temps de trajet trop important pour relier les 2 gares. Mêmesi nous reconnaissons qu’une desserte des Atlantes et des espaces deRochepinard (parc expos, le stade, et les surfaces commerciales) estintéressant.

La rencontre du 12 septembre 2017 entre les Représentants deTours-Métropole, sera donc l’occasion pour l’UD-CFDT37 de réaffirmer notrevolonté de voir notre projet de nouveau mis en débat avec des comparatifs decoûts et de délais d’exécution rapide. Pour rappel notre projet a été présentéen décembre 2013, aujourd’hui il pourrait être déjà en service, si les élusavaient évité de polémiquer et de tergiverser sur les tracés. Pendant ce tempsles voyageurs (abonnés, occasionnels et touristes) doivent se contenter delongues minutes d’attentes à St-Pierre.

Notre projet en ligne et à jour sur notre blog

Blog.navette-tours-saintpierre.fr Ou sur notre page Facebook :CFDT-Crte navettes Tours st Pierre

Pour l’UD – CFDT - 37

Le Secrétaire Départemental - Guy SIONNEAU

Pour le Pilotage du Projet Daniel BERNARD

.. raw:: html

   </p>
