navettes - NR du 8 janvier 2017
###############################
:date: 2017-01-09 09:12


:slug: navettes-nr-du-8-janvier-2017
:status: published

NR 37 du dimanche 8 janvier 2017

Plaidoyer pour une navette Alors que le téléphérique de Brest a du plombdans l'aile et que le tramway vers saint-Pierre-des-Corps n'est encore qu'unrêve, Daniel Bernard et son syndicat militent toujours pour une navette entreTours et Saint-Pierre-des-Corps, le long des voies ferrées existantes avec desaménagements dont le coût total pourrait se chiffrer entre 10 et 15 M€. Si laSNCF traîne des pieds, Tour(s)plus pourrait se laisser tenter. Une réunion detravail est prévue le 24 janvier avec Frédéric Augis, en charge des transportsau sein de Tour(s)plus.

.. raw:: html

   </p>
