Rencontre avec Philippe Chalumeau et réponse de Frédéric Augis : ça bouge dans le bon sens
##########################################################################################
:date: 2018-03-05 13:08
:category: Presse et médias


:slug: rencontre-avec-philippe-chalumeau-et-reponse-de-frederic-augis-ca-bouge-dans-le-bon-sens
:status: published

La CFDT a rencontrée, le 2 mars 2018, le député Philippe Chalumeau. Ilrejoint son analyse sur la desserte actuelle et estime le projet porté commesimple, économe en espace et plutôt facile à réaliser. Il s'est engagé à faireavancer le dossier auprès des différentes instances (ministère, métropole,SNCF). Les prochains responsables qui seront rencontrés sont Frédéric Augis(pas de date encore communiqué), Sophie Auconie et Pierre Louault (le 16 avril2018). La députée Fabienne Colboc a également sollicité une rencontre, sansdate fixée pour le moment.

`Communiqué de presse du 4mars 2018 <{static}/docs/com_presse_du_4_mars_2018.pdf>`__

.. raw:: html

   </p>
