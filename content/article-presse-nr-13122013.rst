article Presse NR 13/12/2013
############################
:date: 2015-08-10 06:04
:category: Presse et médias


:slug: article-presse-nr-13122013
:status: published

transport - Le retour des navettes entre Tours et Saint-Pierre - 13/12/201312:35

Supprimées depuis décembre 2011, les navettes permanentes entre les gares deTours et Saint-Pierre manquent aux usagers. L'union départementale CFDT proposeune alternative. Les navettes permanentes entre les gares de Tours etSaint-Pierre, ici les petits-gris désormais supprimés, manquent aux usagers Lesnavettes permanentes entre les gares de Tours et Saint-Pierre, ici lespetits-gris désormais supprimés, manquent aux usagers&nbsp;-&nbsp;(Photo archives NR)

Des liaisons permanentes entre Tours et Saint-Pierre ? Actuellement, iln'y en a plus. La SNCF vous propose de monter dans un TER ou dans un traingrandes lignes et de descendre à la gare suivante, à Tours ou Saint-Pierre.L'union départementale CFDT a fait plancher des cheminots pour proposer unesolution alternative. En utilisant des emplacements déjà dégagés et situés surdes emprises ferroviaires, le comité régional des transports et del'environnement CFDT et le syndicat cheminot du Centre ont imaginé construireune voie dédiée à ces navettes.

Réduire le temps de liaison

Sans rétablir les « petits-gris » désormais obsolètes,l'utilisation de machines bientôt déclassées en région parisienne pourraitpermettre une visibilité aujourd'hui manquante. Actuellement, il faut près devingt minutes pour rallier les deux gares en mode ferroviaire. Une navettedédiée, peut-être automatique comme l'a déjà évoqué Jean Germain pour letronçon carrefour de Verdun - Saint-Pierre, réduirait le temps de liaison. Pourl'instant, le projet n'est pas chiffré.

Les cheminots proposent d'utiliser des emplacements déjà dégagés :

::

       En rose, les emplacements déjà dégagés   En vert, les zones à aménager ou élargir.

.. raw:: html

   </p>
