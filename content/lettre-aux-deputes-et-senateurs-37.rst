lettre aux députés et senateurs 37
##################################
:date: 2018-02-05 12:50
:category: Courriers Institutions


:slug: lettre-aux-deputes-et-senateurs-37
:status: published

Union Départementale CFDT d’Indre et Loire « La Camusière » 18 ruede l’oiselet 37550 Saint Avertin

Tél : 02 47 36 58 58 Fax : 02 47 36 58 51 Email :indre-loire@centre.cfdt .fr

Tours le 5 février 2018

Objet : Desserte des gares de Tours – St-Pierre des Corps Par navette –Projet initial - CFDT Et variante (en page 4)

Lettre à Mesdames et Messieurs les Sénateurs (es) et Députés (ées) d’indreet Loire

Madame, Monsieur,

Depuis des années, les usagers de la SNCF rencontrent des difficultés pourrejoindre les gares de Tours ou St-Pierre des Corps en raison de la suppressiondes navettes. Aujourd’hui, il faut parfois compter un temps d’attente et deparcours entre ces 2 gares proche des 15 à 20 minutes qui viennent doncs’ajouter au temps de trajet.

L’Union Départementale CFDT a souhaité porter au débat le sujet crucial dela desserte entre les gares de Tours et St-Pierre des Corps et cela depuisdécembre 2013, dans le cadre d’un projet de desserte sur voie dédiée économe entermes de temps, d’espace, un projet de circulation économe en énergie. Nousrappelons que le projet de la CFDT, vise avant tout la relation entre les 2gares de Tours et de St-Pierre, ainsi que le quartier de la gare de St-Pierreet ne veut en aucun cas se substituer à un projet indispensable, mais plusambitieux et plus couteux soutenu par la ville de St-Pierre des Corps d’unedesserte par Tram de l’ensemble de la commune de St-Pierre par une nouvelleligne de Tram.

En effet, les métropoles de Tours et d’Orléans, font partie desparticularités du réseau SNCF, avec 2 gares héritées de l’histoire ferroviaire.Cette problématique pose de nombreux problèmes de mobilités et de compréhensionpour les usagers. Il faut donc gérer cette situation en conséquence

Pour la CFDT, le développement de l’image, du dynamisme, desactivités ; économique, sociales et industrielles, culturelles ettouristiques de la Métropole passent par une relation rapide, fonctionnelle etclairement identifiable pour relier rapidement ces deux gares en 5 minutes.C’est en ce sens que notre projet trouve sa pertinence.

Cette contribution a trouvé un écho favorable auprès de votre prédécesseur,malgré les réticences de la SNCF. Il a reçu l’aval de l’association des« navetteurs » du TGV et également un bon accueil auprès del’ADTT.

Nous sommes prêts à vous présenter notre projet dont la presse s’est faitl’écho à plusieurs reprises,

Projet initial de Décembre 2013

Dans l’attente de pouvoir vous rencontrer, recevez l’expression de notrehaute considération.

  Projet initial - CFDT : afin de relier les 2 gares par un systèmeléger et fonctionnel sur voie dédiée, il serait possible ;

1) D’utiliser une voie n°8 (repères B et C) de la SNCF qui ne sert plusactuellement, située le long des résidences hôtelières de la rue Edouardvaillant. La plateforme de cette voie débute au niveau de la gare de Tours prèsdu passage d’accès de la rue Edouard Vaillant (accès minute) (photos 4 24) etse termine sur un « buttoir » situé lui à quelques dizaines de mètresdu pont de la rue Edouard vaillant (au niveau de l’arrêt Fil-Bleu Jolivet -photo 24). La présence de cette voie et donc de la plateforme existe, elle estdisponible au moins jusqu’au pont de la Place Jolivet.

2) Pour permettre des croisements ou garages entre 2 rames, il seraitpossible d’aménager une portion de 2 éme voie dans la pointe des installationsdu CE-SNCF (les Peupliers) dont la SNCF/RFF disposent d’un terrain en fricheactuellement.

3) Il conviendrait aussi d’élargir le pont de la rue Edouard Vaillant de 35m pour permettre l’aménagement d’une nouvelle voie. (Photos 18 à 23)

4) D’utiliser les emprises disponibles de la SNCF le long de la rue GeorgesCollon (dont les maisons vont être détruites), il n’y aurait donc pasd’expropriation. La SNCF dispose d’une piste le long de cette artère etl’élargissement ne poserait pas de problèmes particuliers pour agrandir laplateforme (repères D – Photos 30 à 33 et 41 à 43)

5) Le passage sous le pont de l’autoroute ne doit pas poser de problème nonplus du fait des espaces disponibles d’autant que la piste SNCF y est présente.Il serait donc possible de prolonger cette voie en utilisant une partie desemprises située sous le pont de l’autoroute et de la prolonger au-dessus du(tube/tunnel) (Photos 46 à 49 repères D et E).

• Voir à partir de ce point la variante au projet côté Sud (A – B – C)

Suite du projet par le Côté Nord 6) Afin de prolonger la voie il seraitpossible d’utiliser les emprises de la SNCF au niveau du parking dutechnicentre de St-Pierre des Corps puis au Nord de ce parking l’espace qui esttoujours disponible (là où la SNCF détenait par le passé un foyer d’accueil« dit du canal ») (Repère G et H – Photos 38 à 40), terrain qui n’est pasutilisé. Cela amènerait à 200/300 mètres de la gare de St-Pierre des Corps, auniveau du transformateur situé au niveau des voies d’accès du technicentre(repère T).

Reste à résoudre au niveau des emprises les quelques 200 à 300 mètres quipermettraient d’accéder directement à la gare de St-Pierre, pour lesquels ilfaudra rechercher des solutions techniques (repère E et F).

Sur l’ensemble de ce tracés, au niveau des emprises, dès lors que SNCFRéseau s’accorderait soit à réaliser les travaux, soit à concéder cette partiede l’infrastructure, les seules véritable difficultés résident surl’élargissement du pont de la rue Edouard Vaillant (au niveau de la placeJolivet), le passage sous l’autoroute et sur les quelques derniers 200 à 300mètres d’accès à la gare de St-Pierre des Corps.

Selon la CFDT, il serait donc possible de réaliser à un coût raisonnable uneligne pour l’aménagement d’un système de transport sur voie ferrée dédiée poury faire circuler un type de rames banlieue rénovées (aujourd’hui placées survoies de garages, donc inutilisées) ou avec du matériel plus léger qui reste àdéfinir (tram, tram-train, métro) entre les deux gares, ce point resterait dela responsabilité du gestionnaire de l’infrastructure (SNCF Réseau) et del’exploitant qui serait retenu dans le cadre de ce projet.

Pour ce faire, il conviendrait que les différents acteurs en charge ceprojet, lancent une étude de faisabilité technique et financière. Par la suite,réaliser un tour de table financier afin de réunir les fonds nécessaire auxinvestissements. A cette fin, l’agglo pourrait recourir à mettre en place une« surtaxe locale temporaire », voir le document joint en annexe.

• Variante (accès gare St-Pierre par le Sud).

Depuis les divers rencontres que nous avons organisées, nous avons poursuivinotre travail sur le projet. Aujourd’hui nous avons décidé de présenter unevariante à notre projet initial (par le côté Sud (Sernam) de St-Pierre), quiaurait pour avantage de permettre un accès à la gare de St-Pierre des Corpsdirectement par le souterrain déjà relié par un ascenseur et un escalier. Cettevariante n’aurait pas ou peu d’incidence sur l’estimation globale des coûtsexposés précédemment compte tenu des infrastructures déjà existantes et del’espace disponible.

A) Pour cela, nous proposons, la construction d’un pont rail de 25 mètres delong sur une hauteur de 5,50 à 6,00 m, portant une seule voie, enjambant ainsiles 4 voies ferrées (voir schémas) nécessitant une rampe d’accès de part etd’autres d’environ 70 à 80m minimum pour une déclivité d’environ 6 à 7%.

B) Cette voie longerait ensuite d’anciennes voies désaffectées de l’ex-courmarchandises (le long de la voie 1 «’dite de Bordeaux’). Elle pourrait seterminer au niveau du quai du parking Sud côté SERNAM (voir plan) et bénéficierainsi d’un accès direct au souterrain (prévu avec un débouché, initialementpour une voie supplémentaire).

Pour toutes ces raisons, nous aimerions que vous nous accordiez quelquesinstants pour en débattre.

Nous attirons votre attention sur les nouvelles dispositions prises enapplication du Décret n° 2017-1556 du 10 novembre 2017 relatif au transfert depropriété du domaine public ferroviaire et portant diverses dispositionsrelatives à ce domaine, qui permettent aux collectivités qui le souhaitent deformuler une demande de transfert d’installations ou d’espaces (selon notrepropre analyse du texte à vérifier).

Dans ce cadre, la Métropole pourrait s’appuyer sur le réseau desParlementaires et Sénateurs afin qu’elles, qu’ils interpellent la Ministre surl’application sur ces dispositions et leurs applications dans le casd’espèce.

JORF n°0264 du 11 novembre 2017 - Texte n°42

Variante de janvier 2018 (accès au souterrain par le Sud à la gare deSt-Pierre)

Restant à votre disposition, recevez l’expression de notre profonddévouement pour l’intérêt de la collectivité et du développement économique ettouristique.

Pour l’UD – CFDT – 37

Le Secrétaire Départemental Guy SIONNEAU

Responsable du Pilotage du Projet :

Daniel BERNARD

Notre projet à consulter en ligne sur notre blog

Blog.navette-tours-saintpierre.fr Ou sur notre page Facebook :CFDT-Crte navettes Tours st Pierre

Copie transmise à : Mme Isabelle RAIMOND-PAVARO – Sénatrice, CD CantonChinon M Serge BABARY Sénateur – Sénateur, conseiller Municipal de Tours PierreLOUAULT – Sénateur, CD canton de Chinon

Mme Sophie AUCONIE – 125, rue de l’université – 75355 – Paris 07 P MmeFabienne COLBOC – 6, rue de la Douzillère – 37300 – Joué les Tours Mme SabineTHILLAYE – 2017 – BP 80105 – 37541 – St Cyr sur Loire cedex M PhilippeCHALUMEAU – 2, Jardin de Beaune – 37000 – TOURS M Daniel LABARONNE – 4, Placede la République – Appt2 – 37150 - Bléré

.. raw:: html

   </p>
