article presse NR 10/11/2014
############################
:date: 2015-08-10 05:55
:category: Presse et médias


:slug: article-presse-nr-10112014
:status: published

Faut-il des navettes entre Tours et Saint-Pierre ? 10/11/2014 05:46

Les fameux wagons baptisés les « petits gris » ont relié pendantdes décennies les gares de Tours et de Saint-Pierre-des-Corps jusqu'à leurarrêt, en décembre 2011. - Les fameux wagons baptisés les « petitsgris » ont relié pendant des décennies les gares de Tours et deSaint-Pierre-des-Corps jusqu'à leur arrêt, en décembre 2011. - (Photo NR,Patrice Deschamps) Les fameux wagons baptisés les « petits gris » ontrelié pendant des décennies les gares de Tours et de Saint-Pierre-des-Corpsjusqu'à leur arrêt, en décembre 2011. - (Photo NR, Patrice Deschamps) suivanteprécédente Les fameux wagons baptisés les « petits gris » ont reliépendant des décennies les gares de Tours et de Saint-Pierre-des-Corps jusqu'àleur arrêt, en décembre 2011. - (Photo NR, Patrice Deschamps)

Les navettes systématiques entre les gares de Tours ont été supprimées il ya trois ans. La CFDT propose d’y revenir, avec une voie dédiée. Le projetpiétine.

Depuis la disparition, fin 2011, des navettes en aluminium, les fameuxwagons « petits-gris », qui reliaient les gares de Tours etSaint-Pierre-des-Corps, il n'y a plus de liaison ferroviaire dévolue à cettedesserte. Les usagers du TGV doivent compter sur les correspondances pourrallier les deux gares, ce qui prend en moyenne une vingtaine de minutes.« Alors qu'il faut 55 minutes pour aller à Paris par le TGV. C'estinacceptable. C'est pourquoi nous travaillons sur le projet d'une ligne dédiéeà cette liaison depuis deux ans », indique le secrétaire régional des cheminotsretraités de la CFDT, Daniel Bernard.

Il est à la tête d'un groupe de travail qui a élaboré un projet d'un systèmede desserte en site propre entre les deux stations, qui utiliserait lesemprises ferroviaires disponibles.

Un projet à 10-15 millions d'euros

Les divers projets échafaudés par l'agglomération et la SNCF, ces dernièresannées, ont abouti à des scénarios coûteux, de l'ordre de plusieurs dizaines demillions d'euros, qui reposaient sur la création d'une voie dédiée entre lesdeux gares, sur les 2,5 km du tronçon. « Notre projet, lui, coûterabeaucoup moins cher. Entre 10 et 15 millions d'euros hors matériel. Car ilutilise une voie au départ de Tours, celle qui longe les hôtels, et qui ne sertà rien. Cette voie vient s'arrêter sur un butoir après le pont qui enjambe larue Jolivet » précise Daniel Bernard. Pour prolonger ce segment, le groupede travail préconise la création d'une voie, du pont de la rue Jolivet jusqu'àla gare de Saint-Pierre-des Corps. Seul point délicat du tracé proposé par laCFDT, un passage très exigu entre deux pilastres du pont de l'autoroute A 10, àl'ouest de la zone commerciale des Atlantes. « La construction d'unenouvelle ligne servira aussi bien aux voyageurs qu'aux habitants deSaint-Pierre et de Tours. Réseau Ferré de France ne se dit pas hostile auprincipe » assure le cheminot retraité. Mais le projet piétine. « Lescontacts que nous avons eus avec les élus de Tour(s)plus, avec Jean Germain,Marie-France Beaufils, Frédéric Thomas, restent sans réponse », déplore DanielBernard. « Pourtant, c'est un projet rapide et moins cher que tous lesautres qui se ferait sans expropriations, sans déclaration d'utilité publique.Le seul point critique c'est ce passage en " s " sous le pont ». Et, d'aprèslui, « la demande de la population est très forte. Chaque jour, la gare deSaint-Pierre-des-Corps accueille 300 circulations ferroviaires. Et selon lesjours, on compte entre 1.000 et 3.000 personnes qui utilisent lescorrespondances entre les deux gares ». Pascal Landré

Suivez-nous sur Facebook

.. raw:: html

   </p>
