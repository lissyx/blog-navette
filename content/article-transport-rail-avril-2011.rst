article Transport Rail Avril 2011
#################################
:date: 2015-08-12 07:40
:category: Presse et médias


:slug: article-transport-rail-avril-2011
:status: published

27 avril 2011 Tours : comment compenser la suppression desnavettes ?

La suppression des navettes Tours - Saint-Pierre-des-Corpsn assurées enZ5300, va générer des désagréments pour les voyageurs des TGV et des Interloiredesservant Saint-Pierre. Le report des voyageurs à destination de Tours sur lestrains existants, Aqualys, TER Centre venant du val de Loire (Orléans et Blois)et de la tranversale (Nevers, Bourges, Bléré-la-Croix) ne sera pas simplepuisque c'est à l'approche de Tours que les trains sont les plus chargés. LaSNCF annonce que certaines navettes seront maintenues, mais assurées avec dumatériel TER, notamment des X73500 d'une capacité limitée à 80 places.

La position des deux gares tourangelles et la perspective de la mise enservice du tramway en 2013 ouvre la porte à de nouvelles réflexions, d'autantplus que le tramway longera le faisceau de la gare de Tours par l'ouest etproposera de ce fait une excellente correspondance avec les trains.

100206_5397tours1

Tours - 10 février 2006 - La navette Tours - Saint Pierre des Corps a delongue date récupéré du matériel de banlieue parisienne en fin de vie. LesZ5300 semblent clore ce chapitre... © transportrail

Un tram-train pour succéder à la navette ?

Ce serait la possibilité "dans le vent", surfant sur la vague du tram-trainpromu par la SNCF. L'objectif serait de faire de la navette une nouvellebranche du réseau urbain tourangeau, à condition de trouver un matériel roulantau gabarit de 2,40 m comme le réseau urbain (ce n'est pas un problème, c'estprévu dans le marché du Dualis) mais aussi compatible avec une alimentation parle sol (et c'est déjà plus compliqué, et surtout plus coûteux). Le terminuspourrait être implanté juste avant le pont Wilson, ou en rive droite de laLoire, en constituant une nouvelle branche, par exemple sur le boulevardCharles de Gaulle. Cependant, les modalités de la transition du tram autram-train et le cisaillement de l'ensemble du faisceau d'avant-gare de Tourspar une desserte fréquence (10 minutes d'intervalle) ne sont pas sans poserquestions de nature à tempérer ce qui semblait être initialement uneévidence.

La gare de l'avenue de Grammont

C'est une autre possibilité : créer une gare sur les voies communes auxlignes Tours - Angers - Nantes, Tours - Le Mans et Tours - Châteaudun - Parispour donner correspondance au futur tramway. Bénéficiaires, les étudiantsinstallés en rive sud du Cher, et les habitants des quartiers sud deTours ; en revanche, un temps de parcours allongé pour rejoindre lecentre-ville et les quartiers historiques, surtout si des diamétralisationsétaient engagées entre des TER venant de ces lignes, notamment celle d'Angers,et ceux d'Orléans et de Bourges. Reste que les flux TGV de Saint Pierre desCorps n'auraient toujours pas de solution viable.

Posté par ortferroviaire à 17:45 - Centre - Commentaires `0 <0>`__ -Permalien `# <#>`__

.. raw:: html

   </p>
