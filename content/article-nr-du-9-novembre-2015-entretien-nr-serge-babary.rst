article NR du 9 novembre 2015 - entretien NR-Serge Babary
#########################################################
:date: 2015-10-09 16:47
:category: Presse et médias


:slug: article-nr-du-9-novembre-2015-entretien-nr-serge-babary
:status: published

" Une seule gare avec deux terminaux " 09/10/2015 05:35

Depuis l'arrêt des navettes entre Saint-Pierre-des-Corps et Tours en 2011,il n'y a plus de desserte régulière entre les deux villes. - Depuis l'arrêt desnavettes entre Saint-Pierre-des-Corps et Tours en 2011, il n'y a plus dedesserte régulière entre les deux villes. - (Photo archives NR) Depuis l'arrêtdes navettes entre Saint-Pierre-des-Corps et Tours en 2011, il n'y a plus dedesserte régulière entre les deux villes. - (Photo archives NR) Depuis l'arrêtdes navettes entre Saint-Pierre-des-Corps et Tours en 2011, il n'y a plus dedesserte régulière entre les deux villes. - (Photo archives NR)

Liaison entre les gares de Tours et de Saint-Pierre-des-Corps, deuxièmeligne de tram, prolongement du périphérique… les dossiers transport ne manquentpas dans l'agglo.

« Le plus urgent, c'est le raccordement Tours - Saint-Pierre »,souligne le maire. Inadmissible selon lui de mettre une heure quarante pouraller de Paris à Tours en raison des temps d'attente une fois arrivé àSaint-Pierre : « Il faut une seule gare avec deux terminaux reliés par unenavette à cadencement régulier du type VAL (*). »

6.000 véhicules/jour sur la route de Rouziers

Le tramway ? « C'est un succès, il y a moins de voitures en ville » ;sur le projet de deuxième ligne, Serge Babary estime que « les centreshospitaliers universitaires, les campus et les zones d'habitat dense »sont des lieux à desservir en priorité. « On peut imaginer des extensionsà partir de la ligne existante vers Trousseau et Grandmont dans un premiertemps, puis Bretonneau et La Riche dans un second. » Reste le cas(compliqué) du périphérique. « La route de Rouziers, c'est aujourd'hui6.000 véhicules par jour, s'inquiète le maire. Il faut de nouvellesinfrastructures pour absorber toutes les nouvelles constructions dans cesecteur et celui des Douets. Si on ne le fait pas, alors il faut arrêter deconstruire ! » Rappelons que fin 2014 l'agglo s'est penché sur le dossierdu périphérique via une étude portant sur un diagnostic des points decongestion au nord et différents scénarios pour y remédier. L'une des solutionspourrait être un court prolongement du périphérique, dont la section nord-ouestse termine route du Mans. Ce prolongement permettrait de déboucher sur l'avenuedu Danemark où des aménagements seraient ensuite envisagés pour sécuriser lestraversées piétonnes et fluidifier la circulation des véhicules jusqu'auraccordement avec l'A10 (rond-point Abel-Gance).

(*) Le véhicule automatique léger est un métro sur pneus créé pour lapremière fois à Lille et aujourd'hui présent à Rennes, Toulouse ou dans lesaéroports parisiens (Orlyval, CDG Val).

.. raw:: html

   </p>
