Que sortira t’il de cette nouvelle étude et du projet CFDT ?
############################################################
:date: 2018-05-24 13:22
:category: Presse et médias


:slug: que-sortira-til-de-cette-nouvelle-etude-et-du-projet-cfdt
:status: published

Nouvelle République du 23 mai 2018.

Du nouveau pour Saint-Pierre
----------------------------

Le Collectif pour un tram à l'est de l'agglomération n'a pas manqué desaluer les avancées suite à la réunion publique Organisée vendredi à Saint-Pierre-des-Corps. Durant cette réunion, les remarques sur l’absence de prise encompte de I'Est tourangeau dans le projet de deuxième ligne ont été nombreuses.Philippe Briand, président de Ia Métropole et Frédéric Augis, vice-présidentaux mobilités ont ainsi annoncé le lancement d'une étude pour un tracé de ligneentre la gare de Tours et Saint-Pierre-des-Corps, une fois l’aval du conseilMétropolitain obtenu. Le collectif se félicite de cette annonce qui constitueune première étape vers la prise en compte des besoins de l’Est del’agglomération dans le schéma de tram de la métropole.

Frédéric Augis s'est par ailleurs engagé à venir présenter l’étude sur cettecinquième branche dans les prochains mois.

.. raw:: html

   </p>
