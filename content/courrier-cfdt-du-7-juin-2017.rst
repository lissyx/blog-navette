courrier CFDT du 7 juin 2017
############################
:date: 2017-06-08 16:31
:category: Courriers CFDT


:slug: courrier-cfdt-du-7-juin-2017
:status: published

Union Départementale CFDT d’Indre et Loire « La Camusière » 18 ruede l’oiselet 37550 Saint Avertin

Tél : 02 47 36 58 58 Fax : 02 47 36 58 51 Email :indre-loire@centre.cfdt .fr

Tours le 8 juin 2017

M Philippe BRIAND Président de Tours Métropole Val de Loire

M Frédéric AUGIS Vice-Président Délégué aux mobilités 60, Avenue MarcelDASSAULT 37206 - TOURS – Cedex 3

M Serge BABARY Maire de TOURS Conseiller communautaire

Objet : liaison entre les gares de Tours et de St-Pierre des Corps

Monsieur,

Pour faire suite à votre courrier en date du 11 mai 2017, nous entendons denouveau vous interpeller sur le projet que l’UD-CFDT 37 vous a déposé etcommenté.

En effet, lors de notre rencontre nous avions évoqué la possibilité departiciper quelques instants à la rencontre commune entre le comité techniquede la Métropole et SNCF-Réseau de façon à expliciter complétement notre projetaux différents experts et par la suite de laisser ces 2 organismes travaillersur ce dossier, d’autant plus que nous n’avons jamais eu l’occasion deprésenter physiquement notre projet à SNCF-Réseau.

Il n’est pas question pour nous d’imposer quoi que ce soit, mais bien dedécrire en quelques minutes le projet, de communiquer sur les éléments en notrepossession et nos différentes sources d’informations. Aussi nous réitéronscette demande de bon sens et de dialogue constructif.

D’autre part, nous venons de prendre connaissance par le journal de la Villede St-Pierre des Corps « La Clarté » de l’étude conduite par ToursMétropole présentée dans le cadre du débat que vous avez organisé avec leConseil Municipal sur les nouvelles lignes de Trams. Permettez-nous de revenirsur les 3 tracés tels qu’ils figurent dans ce document. 2A) au-delà du parcourscomplexe qui passerait par le boulevard Heurteloup, puis la Rabatterie, lamairie, puis la gare de St-Pierre des Corps, il serait intéressant de connaitredans ces conditions le temps de trajet pour relier dans ce contexte la gare deTours à la Gare de St-Pierre des Corps. Comme l’a d’ailleurs souligné l’ADTT,ce tracé ne correspondra pas aux attentes des voyageurs.

2B) ce tracé correspond exactement à l’itinéraire de notre projet, sauf quecelui de la CFDT ne passe pas rue Edouard Vaillant, mais empreinte un voieferroviaire existante, donc sans emprise sur le réseau routier. Alors pourquoivouloir faire passer ce tracé par la rue des ateliers, plutôt que de longer lesvoies ferrées existante comme nous l’avons suggéré, y compris en intégrant leprojet de l’agence d’Urbanisme dans le cadre du concours « passage »,comme nous l’avons déjà développé précédemment avec une possibilité d’un pointd’arrêt à cet endroit. 2C) Là encore, il s’agit d’un parcours complexe et d’untemps de trajet trop important pour desservir les 2 gares, même si nousreconnaissons qu’une desserte des Atlantes et des espaces de Rochepinard estintéressant pour le parc expos, le stade, et les surfaces commerciales.

Pour toutes ces raisons, nous aimerions de nouveaux que vous nous accordiezquelques instants pour débattre en votre présence et de celle de SNCF-Réseaupropriétaire des infrastructures ferroviaires.

Restant à votre disposition, recevez Monsieur le Président, Monsieur leVice-Président en charge des mobilités, l’expression de notre profonddévouement pour l’intérêt de la collectivité et du développement touristique.Au nom de l’UD – CFDT 37, nous vous présentons notre haute considération.

Notre projet en ligne sur notre blog

Blog.navette-tours-saintpierre.fr Ou sur notre page Facebook :CFDT-Crte navettes Tours st Pierre

Pour l’UD – CFDT - 37

Le Secrétaire Départemental - Guy SIONNEAU

Pour le Pilotage du Projet Daniel BERNARD

.. raw:: html

   </p>
