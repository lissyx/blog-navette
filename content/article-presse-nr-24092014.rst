article presse NR 24/09.2014
############################
:date: 2015-08-10 06:15
:category: Presse et médias


:slug: article-presse-nr-24092014
:status: published

Saint-Pierre-des-Corps : la gare doit être repensée - 24/09/201405:46

L'étude montre notamment que 1.750 voitures sont stationnées toute lajournée alors que l'on compte 1.200 places de parking. -

Comment désengorger la gare de Saint-Pierre-des-Corps ? Une étude arécemment été menée auprès des voyageurs. Elle permet d’avancer quelquespistes.

La gare de Saint-Pierre-des-Corps souffre de plusieurs maux. Ledépose-minute est surchargé à certaines heures, les vélos et autres deux-rouesencombrent l'esplanade, les places de stationnement sont insuffisantes etnombre de voyageurs refusant de payer le parking occupent les places des ruesalentours. Pour tenter de résoudre ces problèmes, une étude a été menée enavril auprès des usagers de la gare. Elle a permis de mettre en avant les modesde transports utilisés et ainsi d'envisager des solutions pour améliorer lasituation. Cette enquête a notamment été réalisée un mardi, de 6 h à 20 h.Alors que 11.765 mouvements ont été enregistrés ce jour-là, 4.548 personnes ontété interrogées. « C'est une bonne photo à l'instant T des pratiques desutilisateurs », souligne Marie-France Beaufils, sénatrice-maire deSaint-Pierre-des-Corps.

   > LIRE. L'art de la triple file devant la gare de Saint-Pierre

..

   > LIRE. Le parvis de la gare de plus en plus encombré

Il en ressort, mais ce n'est pas une surprise, que la voiture reste le moyenle plus utilisé pour se rendre à la gare. Et même si certains se font déposer,ce sont tout de même environ 1.750 voitures qui stationnent toute la journée(le mardi) alors que l'on compte 1.200 places de parking (payantes) autour dela gare. « Il y a une part importante d'usagers qui ne veulent pas payer,donc ils s'éloignent », souligne l'élu. Ce qui n'est pas sans poser deproblèmes aux riverains dans certains quartiers, qui, du coup, ont desdifficultés à trouver une place de parking. L'étude montre aussi que lesvoyageurs qui viennent à pied ou à vélo sont essentiellement les habitantsrésidant à proximité de la gare, donc surtout des Corpopétrussiens. MaisMarie-France Beaufils semble convaincue que « si le stationnement desvélos en gare est mieux sécurisé et mieux positionné », les usagers pourraientêtre plus nombreux à choisir ce mode de transport. Les dysfonctionnements sontdonc clairement identifiés : « Surcharge très forte sur le parvis sur ledépose-minute, beaucoup de vélos et de deux-roues qui occupent une partimportante de l'esplanade, des taxis en forte densité à certainsmoments. » Et la sénatrice-maire de penser aux solutions à court terme :« Dans la partie sud de la gare, on pourrait consacrer un endroit austationnement sécurisé des vélos et un au dépose-minute, pour que la pressionbaisse sur la partie nord. » Des aménagements pour faciliter l'accès descyclistes par le sud (sans avoir à passer par le pont Jean-Moulin) pourraientaussi être réalisés afin de développer l'utilisation du vélo. L'élue attendaussi beaucoup de l'étude actuellement menée par l'agglo de Tours sur lestransports en commun. Le bus pourrait aussi être une piste pour désengorger lagare des voitures. Si l'idée est bien de « chercher des alternatives à lavoiture », Marie-France Beaufils précise tout de même qu'un nouveau parkingpayant, à l'est de la gare, devrait être construit (un niveau supérieur seraitréalisé au-dessus de l'existant) : il compterait une centaine de places.Florence Vergne

Suivez-nous sur Facebook

.. raw:: html

   </p>
