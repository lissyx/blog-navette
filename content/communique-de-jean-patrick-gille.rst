communiqué de Jean Patrick GILLE
################################
:date: 2015-08-10 06:34
:category: Courriers Institutions


:slug: communique-de-jean-patrick-gille
:status: published

communiqué de Jean Patrick GILLE

LGV TOURS-BORDEAUX : Vigilants et exigeants, soyons mobilisés pour laTouraine.

Dès fin septembre, j’ai rencontré les représentants de la CFDT transportsqui m’ont présenté leur projet de voie dédiée à des navettes entre TOURS et STPIERRE DES CORPS.

Ce lundi, j’ai rencontré le représentant des abonnés du TGV TOURS-PARIS.Nous avons fait le point sur la desserte actuelle et son avenir dans le cadrede l’ouverture de la LGV. Je lui ai confirmé que je soutiens la création d’uncomité « LGV Touraine 2017″ rassemblant les élus locaux et les forcesvives de notre Département.

Indispensable à des délais respectés et une bonne desserte de TOURS, jesoutiens le projet d’une navette efficace entre TOURS et ST-PIERRE DESCORPS.

Enfin, suite à ces deux échanges, j’ai sollicité un rendez-vous auprès duministre des transports afin d’aborder ce projet de navettes, les tarifsactuels et futurs des billets et abonnements ainsi que l’avenir de la dessertedans le cadre de la mise en service de la LGV.

Sur ce sujet, comme celui du redécoupage des Régions, j’appelle à lamobilisation des Tourangelles et des Tourangeaux et de leurs représentants detous bords pour défendre les intérêts et les atouts de Tours et de la Touraineet valoriser ensemble notre territoire.

.. raw:: html

   </p>
