Réunion avec Frédéric Augis
###########################
:date: 2018-09-21 08:24
:category: Courriers Institutions


:slug: reunion-avec-frederic-augis
:status: published

Suite au courrier sollicitant une nouvelle réunion avec Frédéric Augis,vice-président en charge des mobilités, celui-ci notifie la volonté derencontre pour faire avancer le dossier, comme indiqué dans le `courrier du 14 septembre <{static}/docs/courrier_Briand_14_sept_18.pdf>`__.

.. raw:: html

   </p>
