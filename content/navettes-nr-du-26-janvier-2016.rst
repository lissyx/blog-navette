navettes NR du 26 janvier 2016
##############################
:date: 2016-01-26 17:16
:category: Presse et médias


:slug: navettes-nr-du-26-janvier-2016
:status: published

" Le projet de navette entre les gares de Tours et Saint-Pierre-des-Corpsn'est pas qu'un projet pour les usagers du rail. " NR du 26/01/2016

la phrase

Tandis que le président de l'agglomération – Philippe Briand – vient deconfirmer le lancement de nouvelles études pour la création d'une seconde lignede tram, la CFDT s'invite dans ce dossier pour rappeler l'existence de sonprojet de navettes ferroviaires entre les gares de Tours et Saint-Pierre. Selonle syndicat, ce projet (qui a déjà été présenté à deux reprises au maire deTours) doit s'inscrire dans la réflexion globale sur le développement destransports en commun dans l'agglo. Pour la CFDT, l'extension du tramway versSaint-Pierre-des-Corps ne répondra pas à l'attente des usagers du TGV dans lamesure où les temps de parcours seraient « d'au moins 15 à 20 minutes ».De plus, une liaison tram-train de quai à quai paraît plus délicate àSaint-Pierre compte tenu des constructions aux abords de la gare, alors que80 % du foncier serait déjà disponible pour le projet de navetteferroviaire.

.. raw:: html

   </p>
