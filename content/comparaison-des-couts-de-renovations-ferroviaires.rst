Comparaison des coûts de rénovations ferroviaires
#################################################
:date: 2016-10-31 11:05
:category: Courriers CFDT


:slug: comparaison-des-couts-de-renovations-ferroviaires
:status: published

Suite à la réunion du Conseil Communautaire du 17 octobre 2016, et à laproposition d'une étude indépendance, la CFDT `détaille dans unelettre à l'agglomération Tour(s)Plus <{static}/docs/lettre_agglo_28_octobre_2016_derniere_version.pdf>`__ les coûts de construction et de remiseen état d'autres infrastructures ferroviaires, pour répondre à l'allégation dela SNCF affirmant que le projet est à 100M€. Soit, pour les 2740m de distance àcouvrir entre les PK233,020 et PK235,760, plus de 40M€ du kilomètre, ce qui sesitue entre les coûts de construction des LGV SEA (50M€ du km).

.. raw:: html

   </p>
