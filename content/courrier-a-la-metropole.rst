courrier à la Métropole
#######################
:date: 2018-02-05 12:52
:category: Courriers Institutions


:slug: courrier-a-la-metropole
:status: published

Union Départementale CFDT d’Indre et Loire « La Camusière » 18 ruede l’oiselet 37550 Saint Avertin

Tél : 02 47 36 58 58 Fax : 02 47 36 58 51 Email :indre-loire@centre.cfdt .fr

Tours le 5 février 2018 M Philippe BRIAND Président de Tours Métropole Valde Loire

M Frédéric AUGIS Vice-Président Délégué aux mobilités 60, Avenue MarcelDASSAULT 37206 - TOURS – Cedex 3

Objet : liaison entre les gares de Tours et de St-Pierre des Corps -Présentation de la variante 2

Monsieur, Pour faire suite à nos rencontres, nous aimerions de nouveau vousrencontrer afin d’échanger sur l’évolution de notre projet de desserte entreles gares de Tours et de St-Pierre des Corps.

En effet, depuis notre dernière rencontre un certain nombre de décisions etd’orientations ont été annoncées et prises concernant le développement duréseau de Tram (ligne 2). Dans ce débat, la desserte entre les 2 gares, noussemble être un sujet conflictuel entre certains élus locaux et laMétropole.

Nous rappelons que le projet de la CFDT, vise avant tout la relation entreles 2 gares de Tours et de St-Pierre, ainsi que le quartier de la gare deSt-Pierre et ne veut en aucun cas se substituer à un projet plus ambitieux etplus couteux soutenu par la ville de St-Pierre des Corps d’une desserte parTram de l’ensemble de la commune de St-Pierre par une nouvelle ligne deTram.

En effet, les métropoles de Tours et d’Orléans, font partie desparticularités du réseau SNCF, avec 2 gares hérité de l’histoire ferroviaire.Cette problématique pose de nombreux problèmes de mobilités et de compréhensionpour les usagers. Il faut donc gérer cette situation en conséquence

Pour la CFDT, le développement de l’image, du dynamisme, desactivités ; économique, sociales et industrielles, culturelles ettouristiques de la Métropole passent par une relation rapide, fonctionnelle etclairement identifiable pour relier rapidement ces deux gares en 5 minutes.C’est en ce sens que notre projet trouve sa pertinence.

Depuis les divers rencontres que nous avons organisées, nous avons poursuivinotre travail sur le projet. Aujourd’hui nous avons décidé de présenter unevariante n°2 à notre projet initial (par le côté Sud (Sernam) de St-Pierre),qui aurait pour avantage de permettre un accès à la gare de St-Pierre des Corpsdirectement par le souterrain déjà relié par un ascenseur et un escalier, cettevariante n’ayant pas ou peu d’incidence sur l’estimation global des coûtsexposés précédemment compte tenu de l’infrastructure et de l’espacedisponible.

Pour cela, nous proposons, la construction d’un pont rail de 25 mètres delong sur une hauteur de 5,50 à 6,00 m, portant une seule voie, enjambant ainsiles 4 voies ferrées (voir schémas) nécessitant une rampe d’accès de part etd’autres d’environ 70 à 80m minimum pour une déclivité d’environ 6 à 7%. Cettevoie longerait ensuite d’anciennes voies désaffectées de l’ex-cour marchandises(le long de la voie 1 «’dite de Bordeaux’). Elle pourrait se terminer au niveaudu quai du parking Sud côté SERNAM (voir plan) et bénéficier ainsi d’un accèsdirect au souterrain (prévu avec un débouché, initialement pour une voiesupplémentaire).

Pour toutes ces raisons, nous aimerions de nouveaux que vous nous accordiezquelques instants pour débattre en votre présence et de celle de SNCF-Réseaupropriétaire des infrastructures ferroviaires.

Nous attirons votre attention sur les nouvelles dispositions prises enapplication du Décret n° 2017-1556 du 10 novembre 2017 relatif au transfert depropriété du domaine public ferroviaire et portant diverses dispositionsrelatives à ce domaine, qui permettent aux collectivités qui le souhaitent deformuler une demande de transfert d’installations ou d’espaces (selon notrepropre analyse du texte à vérifier). Dans ce cadre, la Métropole pourraits’appuyer sur le réseau des Parlementaires et Sénateurs afin qu’elles, qu’ilsinterpellent la Ministre sur l’application sur ces dispositions et leursapplications dans le cas d’espèce.

::

                        JORF n°0264 du 11 novembre 2017 - Texte n°42

Restant à votre disposition, recevez Monsieur le Président, Monsieur leVice-Président en charge des mobilités, l’expression de notre profonddévouement pour l’intérêt de la collectivité et du développement économique ettouristique. Au nom de l’UD – CFDT 37, nous vous présentons notre hauteconsidération. Copie transmise à : M Christophe BOUCHET, Maire de TOURS,Conseiller communautaire Mesdames et Messieurs les parlementaires et Sénateurs(37)

Notre projet à consulter en ligne sur notre blog

Blog.navette-tours-saintpierre.fr Ou sur notre page Facebook :CFDT-Crte navettes Tours st Pierre

Pour l’UD – CFDT - 37

Le Secrétaire Départemental Guy SIONNEAU

Pour le Pilotage du Projet

Daniel BERNARD

Variante pour un accès Sud de la gare de St-Pierre

.. raw:: html

   </p>
