Invitation du Vice-Président Délégué aux Mobilités de Tours Métropole Val-de-Loire
##################################################################################
:date: 2017-07-04 05:55
:category: Courriers Institutions


:slug: invitation-du-vice-president-delegue-aux-mobilites-de-tours-metropole-val-de-loire
:status: published

Suite au `courrier du7 juin 2017 </post/2017/06/08/courrier-CFDT-du-7-juin-2017>`__, le Vice-Président Délégué aux Mobilités de Tours MétropoleVal-de-Loire, M Frédéric AUGIS propose un entretien le 12 septembre à propos dela liaison entre les deux gares principales de la métropole.

`Réponse d'invitation de M FrédéricAUGIS <{static}/docs/20170630102333945.pdf>`__

.. raw:: html

   </p>
