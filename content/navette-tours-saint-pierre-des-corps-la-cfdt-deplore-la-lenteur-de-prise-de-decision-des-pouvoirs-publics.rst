Navette Tours-Saint Pierre-des-Corps : la CFDT déplore la lenteur de prise de décision des pouvoirs publics
###########################################################################################################
:date: 2019-01-10 16:58
:category: Presse et médias


:slug: navette-tours-saint-pierre-des-corps-la-cfdt-deplore-la-lenteur-de-prise-de-decision-des-pouvoirs-publics
:status: published

Alors que le projet a été présenté en décembre 2013, voilà 5 ans, lelancement de récentes études par la Métropole pourrait signifier la remise encause d'une desserte ferroviaire efficace des deux gares. La CFDT s'interrogesur la lenteur des réflexions à propos de son projet de navettes sur voiedédiée, sans expropriation. `La NouvelleRépublique du 29 décembre 2018 <{static}/docs/NR_du_29.12.18.pdf>`__

.. raw:: html

   </p>
