Réponse de Serge Babary, Sénateur
#################################
:date: 2018-06-21 08:53
:category: Courriers Institutions


:slug: reponse-de-serge-babary-senateur
:status: published

Suite à la rencontre du 7 avril, la réponse par courrier du Sénateurd'Indre-et-Loire Serge Babary confirmant son intérêt pour le projet etl'implication de la métropole.

`Courrier de réponse de SergeBabary <{static}/docs/courrier_babary.pdf>`__

.. raw:: html

   </p>
