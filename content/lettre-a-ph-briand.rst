lettre a ph BRIAND
##################
:date: 2015-08-09 15:57
:category: Courriers CFDT


:slug: lettre-a-ph-briand
:status: published

Union Départementale CFDT d’Indre et Loire « La Camusière » 18 ruede l’oiselet 37550 Saint Avertin

Tél : 02 47 36 58 58 Fax : 02 47 36 58 51

::

                                Email : indre-loire@centre.cfdt.fr                            Contacts : stic-cfdt-fgte@orange.fr                                               bernard.daniel.37@wanadoo.fr

Tours le 20 avril 2014

Objet : Desserte des gares de Tours – St-Pierre des Corps

Monsieur Philippe BRIAND Président de Tours (+) 60 Avenue Marcel Dassault37200 - Tours

Monsieur,

Comme vous le savez, depuis de nombreuses années, les usagers de la SNCF seplaignent de la disparition des navettes reliant les gares de Tours et deSt-Pierre des Corps. Aujourd’hui, il faut souvent ajouter au temps de trajetprincipal parfois entre 15 et 20 minutes dans le cadre des correspondances engare de St-Pierre, ce qui vous l’avouerait interpelle lorsque pour faire les230 kms qui séparent St-Pierre des corps à Paris, peux se faire en 1 heure.

L’Union Départementale CFDT en lien avec son C.R.T.E\* (comité Régional desTransports et de l’Environnement – CFDT) et le Syndicat des cheminots de laRégion Centre, ont réalisé un projet réaliste qui permettrait à un coûtraisonnable pour la collectivité de relier ces 2 gares par un mode de transportsur voie ferrée dédié en utilisant une grande partie de l’infrastructureexistante, aujourd’hui propriété de RFF/SNCF.

Au moment de choix importants à la fois dans le cadre du PDU del’agglomération de Tours et du futur Contrat de Plan, il nous a semblé opportunde publier et de mettre en débat la proposition de la CFDT d’Indre et Loire etainsi interpeller les différents acteurs politiques et économiques.

Vous trouverez ci-joint le dossier élaboré par nos soins avec d’une part ledocument de travail et d’autre part un CD/DVD qui contient photos et pal de ceprojet.

En espérant que vous porterez un regard attentif à ce projet, nous sommesdisposés à rencontrer vos services pour en débattre.

Veuillez agréer, Monsieur le Président de Tours (+), l’expression de notrehaute considération.

Contact : stic-cfdt-fgte@orange.fr

Pour l’UD – CFDT – 37 Le Secrétaire Guy SIONNEAU Pour le CRTE Centre – CFDTLe Secrétaire Claude GAROU

Pour le Syndicat des Cheminots CFDT et FGAAC-CFDT Centre La SecrétaireCorinne DUPONT Pour le Syndicat CFDT des Transports Urbains Le SecrétairePatrick CHAUVE Pour L’union Régionale des Retraités des Transports et del’Environnement CFDT – Centre Le Secrétaire Daniel BERNARD

Pour le Syndicat des Cheminots CFDT et FGAAC-CFDT Centre La SecrétaireCorinne DUPONT

-  Le CRTE est composé du :

Syndicat CFDT des Autoroutes Syndicat CFDT des Transports Urbains SyndicatCFDT des Cheminots région Centre et FGAAC-CFDT Syndicat Interdépartemental desTransports Centre (STIC-CFDT) Syndicat de l’Environnement et de l’EquipementUnion Régionale des Retraités des Transports et de l’Environnement

.. raw:: html

   </p>
