Texte concernant l'instauration d'une surtaxe locale temporaire
###############################################################
:date: 2015-09-07 10:20
:category: Projet


:slug: texte-concernant-linstauration-dune-surtaxe-locale-temporaire
:status: published

Texte détaillant la proposition d'instauration d'une surtaxe localetemporaire destinée à financer la liaison entre les gares de Tours etSaint-Pierre-des-Corps.

`surtaxe locale temporaire <{static}/docs/20150812112142454.pdf>`__

.. raw:: html

   </p>
