Navette Tours-Saint Pierre : « La CFDT croit plus que jamais en son projet »
############################################################################
:date: 2017-11-22 09:48
:category: Presse et médias


:slug: navette-tours-saint-pierre-la-cfdt-croit-plus-que-jamais-en-son-projet
:status: published

La CFDT et les élus se rejoignent sur le projet de navette, et les récentschangements à la direction de SNCF Mobilités et SNCF Réseaux pourraientpermettre de débloquer une situation coincée depuis six ans maintenant.

`ArticleInfo-Tours.fr <{static}/docs/Navette_Tours_article_info-tours.pdf>`__ `Article Info-Tours.fr <http://www.info-tours.fr/articles/tours-agglo/2017/11/21/7485/navette-tours-saint-pierre-la-cfdt-croit-plus-que-jamais-en-son-projet/>`__

.. raw:: html

   </p>
