Projet Power Point
##################
:date: 2015-08-10 11:19
:category: Courriers CFDT


:slug: projet-power-point
:status: published

**Pour découvrir le projet de la CFDT-37 concernant les navettes ToursSt-Pierre, vous pouvez désormais aller sur notre blog à l'adresse suivante:**

navette-tours-saintpierre.fr

Cette adresse est a inscrire dans la barre de navigation a la place de -https:// etc

dans ce blog, vous découvrirez les différents courriers adressés aux élus etautres institutions, les articles de presse, les liens videos, etc...

vous pourrez également découvrir le POWER POINT concernant la présentationde notre projet dans la rubrique ; Projet et ensuite cliquer surPrésentation projet pour ouvrir le Power Point

la CFDT vous remercie de laisser vos commentaires et suggestions.

Nous vous tiendrons informés des suites de ce dossier à l'issue de larencontre avec Tous Plus et le Maire de tours le 5 septembre

le pilote du projet

daniel BERNARD

.. raw:: html

   </p>
